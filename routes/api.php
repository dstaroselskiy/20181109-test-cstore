<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

/* Rout group for work with model Card */
Route::group(['prefix' => '/cards', 'middleware' => 'auth:api'], function() {
    Route::get('/', 'Api\CardController@index')
        ->name('api-card-list');
    Route::post('/', 'Api\CardController@store')
        ->middleware('can:create,App\\Models\\Card')
        ->name('api-card-create');
    Route::get('/{card}', 'Api\CardController@show')
        ->where('card', '[0-9]+')
        ->middleware('can:view,card')
        ->name('api-card-show');
    Route::get('/{card}/activate', 'Api\CardController@activate')
        ->where('card', '[0-9]+')
        ->middleware('can:update,card')
        ->name('api-card-activate');
    Route::get('/{card}/disable', 'Api\CardController@disable')
        ->where('card', '[0-9]+')
        ->middleware('can:update,card')
        ->name('api-card-disable');
    Route::put('/{card}', 'Api\CardController@update')
        ->where('card', '[0-9]+')
        ->middleware('can:update,card')
        ->name('api-card-update');
    Route::delete('/{card}', 'Api\CardController@destroy')
        ->where('card', '[0-9]+')
        ->middleware('can:delete,card')
        ->name('api-card-delete');
});
/* Rout group for work with model Commit by Card */
Route::group(['prefix' => '/cards/{card}/commits', 'middleware' => 'auth:api'], function() {
    Route::get('/', 'Api\CommitController@index')
        ->where('card', '[0-9]+')
        ->middleware('can:view,card')
        ->name('api-commit-list');
    Route::post('/', 'Api\CommitController@store')
        ->where('card', '[0-9]+')
        ->middleware([
            'can:view,card',
            'can:create,App\\Models\\Commit'
        ])
        ->name('api-commit-create');
    Route::get('/{commit}', 'Api\CommitController@show')
        ->where('commit', '[0-9]+')
        ->where('card', '[0-9]+')
        ->middleware([
            'can:view,card',
            'can:view,commit'
        ])->name('api-commit-show');
    Route::put('/{commit}', 'Api\CommitController@update')
        ->where('commit', '[0-9]+')
        ->where('card', '[0-9]+')
        ->middleware([
            'can:view,card',
            'can:update,commit'
        ])->name('api-commit-update');
    Route::delete('/{commit}', 'Api\CommitController@destroy')
        ->where('commit', '[0-9]+')
        ->where('card', '[0-9]+')
        ->middleware([
            'can:view,card',
            'can:delete,commit'
        ])->name('api-commit-delete');
});

/* Rout group for work with model Tag */
Route::group(['prefix' => '/tags', 'middleware' => 'auth:api'], function() {
    Route::get('/', 'Api\TagController@index')
        ->name('api-tag-list');
    Route::post('/', 'Api\TagController@store')
        ->name('api-tag-create');
    Route::get('/{tag}', 'Api\TagController@show')
        ->where('tag', '[0-9]+')
        ->name('api-tag-show');
    Route::put('/{tag}', 'Api\TagController@update')
        ->where('tag', '[0-9]+')
        ->name('api-tag-update');
    Route::delete('/{tag}', 'Api\TagController@destroy')
        ->where('tag', '[0-9]+')
        ->name('api-tag-delete');
    Route::get('/userstags', 'Api\TagController@userstags')
        ->where('tag', '[0-9]+')
        ->name('api-tag-userstags');
});
