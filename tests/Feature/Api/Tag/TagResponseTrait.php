<?php

namespace Tests\Feature\Api\Tag;

/**
 * Prototype for test api request for getting tag information
 *
 * @package Tests\Feature\Api\Tag
 */
trait TagResponseTrait
{
    /**
     * return url for test
     *
     * @return string
     */
    public function getUrl(): string
    {
        return 'api/tags';
    }

    /**
     * return response structure by tag list
     *
     * @return array
     */
    public function getResponseStructureByModelList(): array
    {
        return [
            'data' => [
                '*' => [
                    'id',
                    'name',
                ]
            ],
            'success'
        ];
    }

    /**
     * return response structure by tag info
     *
     * @return array
     */
    public function getResponseStructureByModel(): array
    {
        return [
            'data' => [
                'id',
                'name',
            ],
            'success'
        ];
    }



}
