<?php

namespace Tests\Feature\Api\Tag;

use App\Models\Tag;
use App\Models\User;
use Barryvdh\Reflection\DocBlock\Type\Collection;
use Tests\Feature\Api\ApiTestPrototype;
use JWTAuth;

/**
 * Test api request for getting tag information
 *
 * @package Tests\Feature\Api\Tag
 */
class TagByUserTest extends ApiTestPrototype
{
    use TagResponseTrait;
    /** @var User */
    public static $user;
    /** @var string */
    public static $user_token;
    /** @var Collection */
    public static $tags;
    /**
     * Prepare data's for testing
     *
     * @return void
     */
    public function prepareDataForTest(): void
    {
        if( empty( self::$user ) ){
            /** @var Collection $users */
            self::$user = factory(User::class)->create();
            self::$user_token = \JWTAuth::fromUser(self::$user);
        }
        if( empty( self::$tags ) ){
            /* Create tags for card */
            self::$tags = factory(Tag::class, 4)->create();
        }
    }

    /**
     * Delete Data's after test
     * @afterClass
     */
    public static function deleteData(): void
    {
        if( !empty( self::$user ) ){
            self::$user->forceDelete();
        }
        if( !empty( self::$tags ) ){
            self::$tags->each(function(Tag $tag){$tag->forceDelete();});
        }
    }

    /**
     * return header for request by user
     *
     * @return array
     */
    public function getHeaderForUser():array
    {
        return [
            'Accept' => 'application/json',
            'Authorization' => 'Bearer '.self::$user_token,
        ];
    }


    /**
     * Test Getting tags list by user
     *
     * @test
     * @testdox Getting tags list by user
     * @return void
     */
    public function getListOfModels(): void
    {
        $tags_count = Tag::all()->count();
        $this->get(
            $this->getUrl(),
            $this->getHeaderForUser()
        )->assertStatus(200)
            ->assertJsonFragment($this->getResponseSuccess())
            ->assertJsonStructure($this->getResponseStructureByModelList())
            ->assertJsonCount($tags_count,'data');
    }

    /**
     * Test Storing new tag by user
     * @test
     * @testdox Storing new tag by user
     * @return Tag
     */
    public function storeTagByUserAuth(): Tag
    {
        $tag_data = factory(Tag::class)->raw();
        $response_data = $this->post(
            $this->getUrl(),
            $tag_data,
            $this->getHeaderForUser()
        )->assertStatus(201)
            ->assertJsonStructure($this->getResponseStructureByModel())
            ->assertJsonFragment($this->getResponseSuccess())
            ->decodeResponseJson('data');

        $this->assertNotEmpty( $response_data['id'] );
        $this->assertEquals( $tag_data['name'], $response_data['name']);

        $tag = Tag::find( $response_data['id'] );
        $this->assertInstanceOf( Tag::class, $tag);
        $this->assertEquals( $tag->id, $response_data['id']);
        $this->assertEquals( $tag->name, $response_data['name']);

        return $tag;
    }

    /**
     * Test Getting tag by user
     * @test
     * @testdox Getting tag by user
     * @depends storeTagByUserAuth
     * @param $tag Tag
     * @return void
     */
    public function getTagOfUserAuth(Tag $tag): void
    {
        /** @var Tag $tag */
        $response_data = $this->get(
                $this->getUrl().'/'.$tag->id,
                $this->getHeaderForUser()
            )->assertStatus(200)
            ->assertJsonFragment($this->getResponseSuccess())
            ->assertJsonStructure($this->getResponseStructureByModel())
            ->decodeResponseJson('data');

        $this->assertEquals( $tag->id, $response_data['id']);
        $this->assertEquals( $tag->name, $response_data['name']);
    }

    /**
     * Test Updating new tag by user
     * @test
     * @testdox Updating new tag by user
     * @depends storeTagByUserAuth
     * @param $tag Tag
     * @return Tag
     */
    public function updateTagByUserAuth(Tag $tag): Tag
    {
        $tag_data = factory(Tag::class)->raw();
        $tag_data['_method'] = 'PUT';
        $response_data = $this->post(
            $this->getUrl().'/'.$tag->id,
            $tag_data,
            $this->getHeaderForUser()
        )->assertStatus(200)
            ->assertJsonStructure($this->getResponseStructureByModel())
            ->assertJsonFragment($this->getResponseSuccess())
            ->decodeResponseJson('data');

        $this->assertNotEmpty( $response_data['id'] );
        $this->assertEquals( $tag_data['name'], $response_data['name']);

        $tag->refresh();

        $this->assertInstanceOf( Tag::class, $tag);
        $this->assertEquals( $tag->id, $response_data['id']);
        $this->assertEquals( $tag->name, $response_data['name']);
        return $tag;
    }

    /**
     * Test Delete tag by user
     *
     * @test
     * @testdox Delete tag by user
     * @depends updateTagByUserAuth
     * @param $tag Tag
     * @return Tag
     */
    public function deleteTag(Tag $tag): Tag
    {
        $this->delete(
                $this->getUrl().'/'.$tag->id,
                [],
                $this->getHeaderForUser()
            )->assertStatus(200)
            ->assertJsonFragment( $this->getResponseSuccess() );

        $tag->refresh();
        $this->assertTrue( $tag->trashed() );
        return $tag;
    }

    /**
     * Test Getting deleted tag by user
     * @test
     * @testdox Getting deleted tag by user
     * @depends deleteTag
     * @param $tag Tag
     * @return void
     */
    public function getDeletedTag(Tag $tag): void
    {
        $this->get(
                $this->getUrl().'/'.$tag->id,
                $this->getHeaderForUser()
            )->assertStatus(404);
        $tag->forceDelete();
    }

}
