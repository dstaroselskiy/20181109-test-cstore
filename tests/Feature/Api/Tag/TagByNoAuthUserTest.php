<?php

namespace Tests\Feature\Api\Tag;

use App\Models\Tag;
use Barryvdh\Reflection\DocBlock\Type\Collection;
use Tests\Feature\Api\ApiTestPrototype;

/**
 * Test api request for getting tag information, by not auth user
 *
 * @package Tests\Feature\Api\Tag
 */
class TagByNoAuthUserTest extends ApiTestPrototype
{
    use TagResponseTrait;
    /** @var Collection */
    public static $tags;
    /**
     * Prepare data's for testing
     *
     * @return void
     */
    public function prepareDataForTest(): void
    {
        if( empty( self::$tags ) ){
            /* Create tags for card */
            self::$tags = factory(Tag::class, 4)->create();
        }
    }

    /**
     * Delete Data's after test
     * @afterClass
     */
    public static function deleteData(): void
    {
        if( !empty( self::$tags ) ){
            self::$tags->each(function(Tag $tag){$tag->forceDelete();});
        }
    }

    /**
     * return header for request by user
     *
     * @return array
     */
    public function getHeaderForUser():array
    {
        return [
            'Accept' => 'application/json',
            'Authorization' => 'Bearer ',
        ];
    }


    /**
     * Test Getting tag list by user without authorisation
     *
     * @test
     * @testdox Getting tag list by user without authorisation
     * @return void
     */
    public function getListOfModels(): void
    {
        $this->get(
            $this->getUrl(),
            $this->getHeaderForUser()
        )->assertStatus(401);
    }

    /**
     * Test Storing new tag by user without authorisation
     * @test
     * @testdox Storing new by user without authorisation
     * @return void
     */
    public function storeTagByUserAuth(): void
    {
        $tag_data = factory(Tag::class)->raw();
        $this->post(
            $this->getUrl(),
            $tag_data,
            $this->getHeaderForUser()
        )->assertStatus(401);
    }

    /**
     * Test Updating new tag by user without authorisation
     * @test
     * @testdox Updating new tag by user without authorisation
     * @depends storeTagByUserAuth
     * @return void
     */
    public function updateTagByUserAuth(): void
    {
        $tag = Tag::inRandomOrder(time())->first();
        $tag_data = factory(Tag::class)->raw();
        $tag_data['_method'] = 'PUT';
        $this->post(
            $this->getUrl().'/'.$tag->id,
            $tag_data,
            $this->getHeaderForUser()
        )->assertStatus(401);
    }

    /**
     * Test Getting tag by user without authorisation
     * @test
     * @testdox Getting tag by user without authorisation
     * @return void
     */
    public function getTagOfUserAuth()
    {
        /** @var Tag $tag */
        $tag = Tag::inRandomOrder(time())->first();
        $this->get(
                $this->getUrl().'/'.$tag->id,
                $this->getHeaderForUser()
            )->assertStatus(401);
    }

    /**
     * Test Delete tag by user without authorisation
     *
     * @test
     * @testdox Delete tag by user without authorisation
     * @return Tag
     */
    public function deleteTag(): void
    {
        /** @var Tag $tag */
        $tag = Tag::withoutTrashed()->inRandomOrder(time())->first();
        $this->delete(
                $this->getUrl().'/'.$tag->id,
                [],
                $this->getHeaderForUser()
            )->assertStatus(401);

        $tag->refresh();
        $this->assertFalse( $tag->trashed() );
    }
}
