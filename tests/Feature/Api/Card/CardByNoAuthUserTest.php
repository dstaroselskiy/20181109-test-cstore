<?php

namespace Tests\Feature\Api\Card;

use App\Models\Card;
use App\Models\Tag;
use App\Models\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\Feature\Api\ApiTestPrototype;

/**
 * Test api request for getting card information by user, who is not authorised
 *
 * @package Tests\Feature\Api\Card
 */
class CardByNoAuthUserTest extends ApiTestPrototype
{
    use CardResponseTrait;
    /** @var User */
    public static $user;
    /** @var Collection */
    public static $cards;
    /**
     * Prepare data's for testing
     *
     * @return void
     */
    public function prepareDataForTest(): void
    {
        if( empty( self::$user ) ){
            self::$user = factory(User::class)->create();
        }
        if( empty( self::$cards ) ){
            /** @var User $user */
            $user = self::$user;
            /* Create tags for card */
            self::$cards = factory(Card::class, 4)->make()->each(function(Card $card)use($user){
                $card->user()->associate($user)->save();
                return $card;
            });
        }
    }

    /**
     * Delete Data's after test
     * @afterClass
     */
    public static function deleteData(): void
    {
        if( !empty( self::$user ) ){
            self::$user->forceDelete();
        }
        if( !empty( self::$cards ) ){
            self::$cards->each(function(Card $card){$card->forceDelete();});
        }
    }
    /**
     * return header for request by user
     *
     * @return array
     */
    public function getHeaderForUser():array
    {
        return [
            'Accept' => 'application/json',
            'Authorization' => 'Bearer gregbrbrhaerehyerheh ',
        ];
    }
    /**
     * Test Getting cards list by user, who is not authorised
     * @test
     * @testdox Getting cards list by user, who is not authorised
     * @return void
     */
    public function testGetListOfModels(): void
    {
        $this->get(
                $this->getUrl(),
                $this->getHeaderForUser()
            )->assertStatus(401);
    }

    /**
     * Test Getting card by user, who is not authorised
     * @test
     * @testdox Getting card by user, who is not authorised
     * @return void
     */
    public function getCardByNoAuthUser()
    {
        /** @var Card $card */
        $card = self::$cards->random()->first();
        $this->get(
                $this->getUrl().'/'.$card->id,
                $this->getHeaderForUser()
            )->assertStatus(401);
    }

    /**
     * Test Storing new card by user, who is not authorised
     *
     * @ test
     * @testdox Storing card tag by user, who is not authorised
     *
     * @return void
     */
    public function storeCardByNotAuthUser(): void
    {
        $card_data = factory(Card::class)->raw();
        $card_data['tags'] = [];
        $this->post(
            $this->getUrl(),
            $card_data,
            $this->getHeaderForUser()
        )->assertStatus(401);
    }

    /**
     * Test Update card by user
     * @test
     * @testdox Update card tag by user
     * @return void
     */
    public function updateCardByNotAuthUser(): void
    {
        /** @var Card $card */
        $card = self::$cards->random()->first();

        $card_data = factory(Card::class)->raw();
        $card_data['tags'] = [];
        $card_data['_method'] = 'PUT';
        $response_data = $this->post(
            $this->getUrl().'/'.$card->id,
            $card_data,
            $this->getHeaderForUser()
        )->assertStatus(401);

    }
    /**
     * Test Change card status on Disable by user, who didn't create them
     * @test
     * @testdox Change card status on Disable by user, who didn't create them
     * @return Card
     */
    public function changeCardStatusOnDisableByNotAuthUser(): Card
    {
        /** @var Card $card */
        $card = self::$cards->random()->first();

        $card->active = true;
        $card->save();

        $this->get(
                $this->getUrl().'/'.$card->id.'/disable',
                $this->getHeaderForUser()
            )->assertStatus(401);

        $card->refresh();
        $this->assertTrue( $card->active );
        return $card;
    }

    /**
     * Test Change card status on Active by user, who didn't create them
     * @test
     * @testdox Change card status on Active by user, who didn't create them
     * @depends changeCardStatusOnDisableByNotAuthUser
     * @param $card Card
     * @return void
     */
    public function changeCardStatusOnActiveByNotAuthUser(Card $card): void
    {
        $card->active = false;
        $card->save();

        $this->get(
                $this->getUrl().'/'.$card->id.'/activate',
                $this->getHeaderForUser()
            )->assertStatus(401);

        $card->refresh();
        $this->assertFalse( $card->active );
    }

    /**
     * Test Delete card by user, who didn't create them
     *
     * @test
     * @testdox Delete card by user, who didn't create them
     * @return void
     */
    public function deleteCardByNotAuthUser(): void
    {
        /** @var Card $card */
        $card = self::$cards->random()->first();
        $this->delete(
                $this->getUrl().'/'.$card->id,
                [],
                $this->getHeaderForUser()
            )->assertStatus(401);

        $card->refresh();
        $this->assertFalse( $card->trashed() );
    }
}
