<?php

namespace Tests\Feature\Api\Card;

use App\Models\Card;
use App\Models\Tag;
use App\Models\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\Feature\Api\ApiTestPrototype;

/**
 * Test api request for getting card information by user, who did't create it
 *
 * @package Tests\Feature\Api\Card
 */
class CardByOtherUserTest extends ApiTestPrototype
{
    use CardResponseTrait;
    /** @var User */
    public static $user;
    /** @var string */
    public static $user_token;
    /** @var User */
    public static $user_other;
    /** @var string */
    public static $user_other_token;
    /** @var Collection */
    public static $cards;
    /** @var Collection */
    public static $tags;
    /**
     * Prepare data's for testing
     *
     * @return void
     */
    public function prepareDataForTest(): void
    {
        if( empty( self::$user ) ){
            self::$user = factory(User::class)->create();
            self::$user_token = \JWTAuth::fromUser(self::$user);
        }
        if( empty( self::$user_other ) ){
            self::$user_other = factory(User::class)->create();
            self::$user_other_token = \JWTAuth::fromUser(self::$user_other);
        }
        if( empty( self::$tags ) ){
            /* Create tags for card */
            self::$tags = factory(Tag::class, 4)->create();
        }
        if( empty( self::$cards ) ){
            /** @var User $user */
            $user = self::$user;
            /* Create tags for card */
            self::$cards = factory(Card::class, 4)->make()->each(function(Card $card)use($user){
                $card->user()->associate($user)->save();
                return $card;
            });
        }
    }

    /**
     * Delete Data's after test
     * @afterClass
     */
    public static function deleteData(): void
    {
        if( !empty( self::$user_other ) ){
            self::$user_other->forceDelete();
        }
        if( !empty( self::$user ) ){
            self::$user->forceDelete();
        }
        if( !empty( self::$tags ) ){
            self::$tags->each(function(Tag $tag){$tag->forceDelete();});
        }
        if( !empty( self::$cards ) ){
            self::$cards->each(function(Card $card){$card->forceDelete();});
        }
    }
    /**
     * return header for request by user
     *
     * @return array
     */
    public function getHeaderForUser():array
    {
        return [
            'Accept' => 'application/json',
            'Authorization' => 'Bearer '.self::$user_other_token,
        ];
    }
    /**
     * Test Getting cards list by user, who did not create them
     * @test
     * @testdox Getting cards list by user, who did not create them
     * @return void
     */
    public function testGetListOfModels(): void
    {
        $this->get(
                $this->getUrl(),
                $this->getHeaderForUser()
            )->assertStatus(200)
            ->assertJsonFragment($this->getResponseSuccess())
            ->assertJsonCount(0,'data');
    }

    /**
     * Test Getting card by user, who did not create them
     * @test
     * @testdox Getting card by user, who did not create them
     * @return void
     */
    public function getCardOfUser()
    {
        /** @var Card $card */
        $card = self::$cards->random(1)->first();
        $this->get(
                $this->getUrl().'/'.$card->id,
                $this->getHeaderForUser()
            )->assertStatus(403);
    }

    /**
     * Test Change card status on Disable by user, who didn't create them
     * @test
     * @testdox Change card status on Disable by user, who didn't create them
     * @return Card
     */
    public function changeCardStatusOnDisableByUserAuth(): Card
    {
        /** @var Card $card */
        $card = self::$cards->random(1)->first();

        $this->get(
                $this->getUrl().'/'.$card->id.'/disable',
                $this->getHeaderForUser()
            )->assertStatus(403);

        $card->refresh();
        $this->assertTrue( $card->active );
        return $card;
    }

    /**
     * Test Change card status on Active by user, who didn't create them
     * @test
     * @testdox Change card status on Active by user, who didn't create them
     * @depends changeCardStatusOnDisableByUserAuth
     * @param $card Card
     * @return void
     */
    public function changeCardStatusOnActiveByUserAuth(Card $card): void
    {
        $card->active = false;
        $card->save();

        $this->get(
                $this->getUrl().'/'.$card->id.'/activate',
                $this->getHeaderForUser()
            )->assertStatus(403);

        $card->refresh();
        $this->assertFalse( $card->active );
    }

    /**
     * Test Update card by user, who didn't create them
     * @test
     * @testdox Update card tag by user, who didn't create them
     * @return void
     */
    public function updateCardByUserAuth(): void
    {
        /** @var Card $card */
        $card = self::$cards->random(1)->first();

        $card_data = factory(Card::class)->raw();
        $card_data['tags'] = [];
        $card_data['_method'] = 'PUT';
        $response_data = $this->post(
            $this->getUrl().'/'.$card->id,
            $card_data,
            $this->getHeaderForUser()
        )->assertStatus(403);

    }
    /**
     * Test Delete card by user, who didn't create them
     *
     * @test
     * @testdox Delete card by user, who didn't create them
     * @return Card
     */
    public function deleteCardByUserAuth(): Card
    {
        /** @var Card $card */
        $card = self::$cards->random(1)->first();
        $this->delete(
                $this->getUrl().'/'.$card->id,
                [],
                $this->getHeaderForUser()
            )->assertStatus(403);

        $card->refresh();
        $this->assertFalse( $card->trashed() );
        return $card;
    }
}
