<?php

namespace Tests\Feature\Api\Card;

use App\Models\Card;
use App\Models\Tag;
use App\Models\User;
use Illuminate\Database\Eloquent\Collection;
use Tests\Feature\Api\ApiTestPrototype;
use JWTAuth;

/**
 * Test api request for getting card information by user, who is authorisated and created it
 *
 * @package Tests\Feature\Api\Card
 */
class CardByUserTest extends ApiTestPrototype
{
    use CardResponseTrait;
    /** @var User */
    public static $user;
    /** @var string */
    public static $user_token;
    /** @var Collection */
    public static $cards;
    /** @var Collection */
    public static $tags;
    /**
     * Prepare data's for testing
     *
     * @return void
     */
    public function prepareDataForTest(): void
    {
        if( empty( self::$user ) ){
            /** @var Collection $users */
            self::$user = factory(User::class)->create();
            self::$user_token = \JWTAuth::fromUser(self::$user);
        }
        if( empty( self::$tags ) ){
            /* Create tags for card */
            self::$tags = factory(Tag::class, 4)->create();
        }
        if( empty( self::$cards ) ){
            /** @var User $user */
            $user = self::$user;
            /** @var Collection $tags */
            $tags = self::$tags->map(function(Tag $tag){return $tag->id;});
            /* Create tags for card */
            self::$cards = factory(Card::class, 4)->make()->each(function(Card $card)use($user, $tags){
                $card->user()->associate($user)->save();
                $card->tags()->sync( $tags );
                return $card;
            });
        }
    }

    /**
     * Delete Data's after test
     * @afterClass
     */
    public static function deleteData(): void
    {
        if( !empty( self::$user ) ){
            self::$user->forceDelete();
        }
        if( !empty( self::$tags ) ){
            self::$tags->each(function(Tag $tag){$tag->forceDelete();});
        }
        if( !empty( self::$cards ) ){
            self::$cards->each(function(Card $card){$card->forceDelete();});
        }
    }

    /**
     * return header for request by user
     *
     * @return array
     */
    public function getHeaderForUser():array
    {
        return [
            'Accept' => 'application/json',
            'Authorization' => 'Bearer '.self::$user_token,
        ];
    }


    /**
     * Test Getting cards list by user, who created them
     *
     * @test
     * @testdox Getting cards list by user, who created them
     * @return void
     */
    public function getListOfModels(): void
    {
        $this->get(
            $this->getUrl(),
            $this->getHeaderForUser()
        )->assertStatus(200)
            ->assertJsonFragment($this->getResponseSuccess())
            ->assertJsonStructure($this->getResponseStructureByModelList())
            ->assertJsonCount(4,'data');
    }

    /**
     * Test Storing new card by user
     * @test
     * @testdox Storing card tag by user
     * @return Card
     */
    public function storeCardByUserAuth(): Card
    {
        $card_data = factory(Card::class)->raw();
        $card_data['tags'] = self::$tags->map(function(Tag $tag){
           return [
               'id' => $tag->id,
               'name' => $tag->name,
           ];
        })->toArray();
        $response_data = $this->post(
                $this->getUrl(),
                $card_data,
                $this->getHeaderForUser()
            )->assertStatus(201)
            ->assertJsonStructure($this->getResponseStructureByModel())
            ->assertJsonFragment($this->getResponseSuccess())
            ->decodeResponseJson('data');

        $this->assertNotEmpty( $response_data['id'] );
        $this->assertEquals( $card_data['name'], $response_data['name']);
        $this->assertEquals( $card_data['content'], $response_data['content']);
        $this->assertEquals( $card_data['type'], $response_data['type']);
        $this->assertCount( self::$tags->count(), $response_data['tags']);
        $this->assertTrue( $response_data['active'] );

        $card = Card::find( $response_data['id'] );
        $this->assertInstanceOf( Card::class, $card);
        $this->assertEquals( $card->id, $response_data['id']);
        $this->assertEquals( $card->name, $response_data['name']);
        $this->assertEquals( $card->content, $response_data['content']);
        $this->assertEquals( $card->type, $response_data['type']);
        $this->assertCount( self::$tags->count(), $response_data['tags']);
        $this->assertTrue( $response_data['active'] );

        return $card;
    }
    /**
     * Test Getting card by user, who created them
     * @test
     * @testdox Getting card by user, who created them
     * @depends storeCardByUserAuth
     * @param $card Card
     * @return Card
     */
    public function getCardOfUserAuth(Card $card): Card
    {
        $response_data = $this->get(
                $this->getUrl().'/'.$card->id,
                $this->getHeaderForUser()
            )->assertStatus(200)
            ->assertJsonFragment($this->getResponseSuccess())
            ->assertJsonStructure($this->getResponseStructureByModel())
            ->decodeResponseJson('data');

        $this->assertEquals( $card->id, $response_data['id']);
        $this->assertEquals( $card->name, $response_data['name']);
        $this->assertEquals( $card->content, $response_data['content']);
        $this->assertEquals( $card->type, $response_data['type']);
        $this->assertCount( self::$tags->count(), $response_data['tags']);
        $this->assertTrue( $response_data['active'] );
        return $card;
    }

    /**
     * Test Change card status on Disable by user, who created them
     * @test
     * @testdox Change card status on Disable by user, who created them
     * @depends getCardOfUserAuth
     * @param $card Card
     * @return Card
     */
    public function changeCardStatusOnDisableByUserAuth(Card $card): Card
    {
        /** @var Card $card */
        $card = self::$cards->random(1)->first();

        $this->get(
                $this->getUrl().'/'.$card->id.'/disable',
                $this->getHeaderForUser()
            )->assertStatus(200)
            ->assertJsonFragment($this->getResponseSuccess());

        $card->refresh();
        $this->assertFalse( $card->active );
        return $card;
    }

    /**
     * Test Getting card with status disable by user, who created them
     * @test
     * @testdox Getting card with status disable by user, who created them
     * @depends changeCardStatusOnDisableByUserAuth
     * @param $card Card
     * @return Card
     */
    public function getDisableCardOfUserAuth(Card $card): Card
    {
        $response_data = $this->get(
                $this->getUrl().'/'.$card->id,
                $this->getHeaderForUser()
            )->assertStatus(200)
            ->assertJsonFragment($this->getResponseSuccess())
            ->assertJsonStructure($this->getResponseStructureByModel())
            ->decodeResponseJson('data');

        $this->assertEquals( $card->id, $response_data['id']);
        $this->assertFalse( $response_data['active'] );
        return $card;
    }
    /**
     * Test Change card status on Active by user, who created them
     * @test
     * @testdox Change card status on Active by user, who created them
     * @depends changeCardStatusOnDisableByUserAuth
     * @param $card Card
     * @return Card
     */
    public function changeCardStatusOnActiveByUserAuth(Card $card): Card
    {
        $this->get(
                $this->getUrl().'/'.$card->id.'/activate',
                $this->getHeaderForUser()
            )->assertStatus(200)
            ->assertJsonFragment($this->getResponseSuccess());

        $card->refresh();
        $this->assertTrue( $card->active );
        return $card;
    }

    /**
     * Test Update card by user
     * @test
     * @testdox Update card tag by user
     * @depends changeCardStatusOnActiveByUserAuth
     * @param $card Card
     * @return Card
     */
    public function updateCardByUserAuth(Card $card): Card
    {
        $card_data = factory(Card::class)->raw();
        $card_data['tags'] = [];
        $card_data['_method'] = 'PUT';
        $response_data = $this->post(
                $this->getUrl().'/'.$card->id,
                $card_data,
                $this->getHeaderForUser()
            )->assertStatus(200)
            ->assertJsonStructure($this->getResponseStructureByModel())
            ->assertJsonFragment($this->getResponseSuccess())
            ->decodeResponseJson('data');

        $this->assertNotEmpty( $response_data['id'] );
        $this->assertEquals( $card_data['name'], $response_data['name']);
        $this->assertEquals( $card_data['content'], $response_data['content']);
        $this->assertEquals( $card_data['type'], $response_data['type']);
        $this->assertCount( 0, $response_data['tags']);
        $this->assertTrue( $response_data['active'] );

        $card = Card::find( $response_data['id'] );
        $this->assertInstanceOf( Card::class, $card);
        $this->assertEquals( $card->id, $response_data['id']);
        $this->assertEquals( $card->name, $response_data['name']);
        $this->assertEquals( $card->content, $response_data['content']);
        $this->assertEquals( $card->type, $response_data['type']);
        $this->assertCount( 0, $response_data['tags']);
        $this->assertTrue( $response_data['active'] );

        return $card;
    }
    /**
     * Test Delete card by user, who created them
     *
     * @test
     * @testdox Delete card by user, who created them
     * @depends updateCardByUserAuth
     * @param $card Card
     * @return Card
     */
    public function deleteCardByUserAuth(Card $card): Card
    {
        $this->delete(
                $this->getUrl().'/'.$card->id,
                [],
                $this->getHeaderForUser()
            )->assertStatus(200)
            ->assertJsonFragment( $this->getResponseSuccess() );

        $card->refresh();
        $this->assertTrue( $card->trashed() );
        return $card;
    }

    /**
     * Test Getting deleted card by user, who created them
     * @test
     * @testdox Getting deleted card by user, who created them
     * @depends deleteCardByUserAuth
     * @param $card Card
     * @return void
     */
    public function getDeletedCardOfUserAuth(Card $card): void
    {
        $this->get(
                $this->getUrl().'/'.$card->id,
                $this->getHeaderForUser()
            )->assertStatus(404);
        $card->forceDelete();
    }

    /**
     * Test Getting cards list by user, who created them, after Deleted one card
     * @test
     * @testdox Getting cards list by user, who created them, after Deleted one card
     * @depends getDeletedCardOfUserAuth
     * @return void
     */
    public function getCardsOfUserAuth(): void
    {
        $this->get(
            $this->getUrl(),
            $this->getHeaderForUser()
        )->assertStatus(200)
            ->assertJsonFragment($this->getResponseSuccess())
            ->assertJsonStructure($this->getResponseStructureByModelList())
            ->assertJsonCount(4,'data');
    }
}
