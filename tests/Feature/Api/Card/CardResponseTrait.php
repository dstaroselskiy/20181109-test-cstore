<?php

namespace Tests\Feature\Api\Card;

/**
 * Prototype for test api request for getting card information
 *
 * @package Tests\Feature\Api\Card
 */
trait CardResponseTrait
{
    /**
     * return url for test
     *
     * @return string
     */
    public function getUrl(): string
    {
        return 'api/cards';
    }

    /**
     * return response structure by card list
     *
     * @return array
     */
    public function getResponseStructureByModelList(): array
    {
        return [
            'data' => [
                '*' => [
                    'id',
                    'type',
                    'name',
                    'content',
                    'active',
                    'tags' => [
                        '*' => [
                            'id',
                            'name'
                        ],
                    ],
                ]
            ],
            'success'
        ];
    }

    /**
     * return response structure by card info
     *
     * @return array
     */
    public function getResponseStructureByModel(): array
    {
        return [
            'data' => [
                'id',
                'type',
                'name',
                'content',
                'active',
                'tags' => [
                    '*' => [
                        'id',
                        'name'
                    ]
                ],
            ],
            'success'
        ];
    }



}
