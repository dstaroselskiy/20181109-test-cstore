<?php

namespace Tests\Feature\Api\Card;

use App\Models\CardList;
use App\Models\Tag;
use App\Models\User;
use Illuminate\Database\Eloquent\Collection;
use Tests\Feature\Api\ApiTestPrototype;
use JWTAuth;

/**
 * Test api request for getting CardList information by user, who is authorisated and created it
 *
 * @package Tests\Feature\Api\Card
 */
class CardListByUserTest extends ApiTestPrototype
{
    use CardResponseTrait;
    /**
     * return response structure by CardList list
     *
     * @return array
     */
    public function getResponseStructureByModelList(): array
    {
        return [
            'data' => [
                '*' => [
                    'id',
                    'type',
                    'name',
                    'content' => [
                        '*' => [
                            'checked',
                            'name'
                        ]
                    ],
                    'active',
                    'tags' => [
                        '*' => [
                            'id',
                            'name'
                        ],
                    ],
                ]
            ],
            'success'
        ];
    }

    /**
     * return response structure by CardList info
     *
     * @return array
     */
    public function getResponseStructureByModel(): array
    {
        return [
            'data' => [
                'id',
                'type',
                'name',
                'content' => [
                    '*' => [
                        'checked',
                        'name'
                    ]
                ],
                'active',
                'tags' => [
                    '*' => [
                        'id',
                        'name'
                    ]
                ],
            ],
            'success'
        ];
    }
    /** @var User */
    public static $user;
    /** @var string */
    public static $user_token;
    /** @var Collection */
    public static $cards;
    /** @var Collection */
    public static $tags;
    /**
     * Prepare data's for testing
     *
     * @return void
     */
    public function prepareDataForTest(): void
    {
        if( empty( self::$user ) ){
            /** @var Collection $users */
            self::$user = factory(User::class)->create();
            self::$user_token = \JWTAuth::fromUser(self::$user);
        }
        if( empty( self::$tags ) ){
            /* Create tags for CardList */
            self::$tags = factory(Tag::class, 4)->create();
        }
        if( empty( self::$cards ) ){
            /** @var User $user */
            $user = self::$user;
            /** @var Collection $tags */
            $tags = self::$tags->map(function(Tag $tag){return $tag->id;});
            /* Create tags for CardList */
            self::$cards = factory(CardList::class, 4)->make()->each(function(CardList $card)use($user, $tags){
                $card->user()->associate($user)->save();
                $card->tags()->sync( $tags );
                return $card;
            });
        }
    }

    /**
     * Delete Data's after test
     * @afterClass
     */
    public static function deleteData(): void
    {
        if( !empty( self::$user ) ){
            self::$user->forceDelete();
        }
        if( !empty( self::$tags ) ){
            self::$tags->each(function(Tag $tag){$tag->forceDelete();});
        }
        if( !empty( self::$cards ) ){
            self::$cards->each(function(CardList $card){$card->forceDelete();});
        }
    }

    /**
     * return header for request by user
     *
     * @return array
     */
    public function getHeaderForUser():array
    {
        return [
            'Accept' => 'application/json',
            'Authorization' => 'Bearer '.self::$user_token,
        ];
    }


    /**
     * Test Getting CardList's list by user, who created them
     *
     * @test
     * @testdox Getting CardList's list by user, who created them
     * @return void
     */
    public function getListOfModels(): void
    {
        $this->get(
                $this->getUrl(),
                $this->getHeaderForUser()
            )->assertStatus(200)
            ->assertJsonFragment($this->getResponseSuccess())
            ->assertJsonStructure($this->getResponseStructureByModelList())
            ->assertJsonCount(4,'data');
    }

    /**
     * Test Storing new CardList by user
     * @test
     * @testdox Storing CardList tag by user
     * @return CardList
     */
    public function storeCardListByUserAuth(): CardList
    {
        $card_data = factory(CardList::class)->raw();
        $card_data['tags'] = self::$tags->map(function(Tag $tag){
           return [
               'id' => $tag->id,
               'name' => $tag->name,
           ];
        })->toArray();
        $response_data = $this->post(
                $this->getUrl(),
                $card_data,
                $this->getHeaderForUser()
            )->assertStatus(201)
            ->assertJsonStructure($this->getResponseStructureByModel())
            ->assertJsonFragment($this->getResponseSuccess())
            ->decodeResponseJson('data');

        $this->assertNotEmpty( $response_data['id'] );
        $this->assertEquals( $card_data['name'], $response_data['name']);
        $this->assertCount( count( $card_data['content'] ), $response_data['content']);
        $this->assertArraySubset( $card_data['content'], $response_data['content']);
        $this->assertEquals( $card_data['type'], $response_data['type']);
        $this->assertCount( self::$tags->count(), $response_data['tags']);
        $this->assertTrue( $response_data['active'] );

        $card = CardList::find( $response_data['id'] );
        $this->assertInstanceOf( CardList::class, $card);
        $this->assertEquals( $card->id, $response_data['id']);
        $this->assertEquals( $card->name, $response_data['name']);
        $this->assertCount( $card->content->count(), $response_data['content']);
        $this->assertArraySubset( $card->content->toArray(), $response_data['content']);
        $this->assertEquals( $card->type, $response_data['type']);
        $this->assertCount( self::$tags->count(), $response_data['tags']);
        $this->assertTrue( $response_data['active'] );

        return $card;
    }
    /**
     * Test Getting CardList by user, who created them
     * @test
     * @testdox Getting CardList by user, who created them
     * @depends storeCardListByUserAuth
     * @param $card CardList
     * @return CardList
     */
    public function getCardListOfUserAuth(CardList $card): CardList
    {
        $response_data = $this->get(
                $this->getUrl().'/'.$card->id,
                $this->getHeaderForUser()
            )->assertStatus(200)
            ->assertJsonFragment($this->getResponseSuccess())
            ->assertJsonStructure($this->getResponseStructureByModel())
            ->decodeResponseJson('data');

        $this->assertEquals( $card->id, $response_data['id']);
        $this->assertEquals( $card->name, $response_data['name']);
        $this->assertCount( $card->content->count(), $response_data['content']);
        $this->assertArraySubset( $card->content->toArray(), $response_data['content']);
        $this->assertEquals( $card->type, $response_data['type']);
        $this->assertCount( self::$tags->count(), $response_data['tags']);
        $this->assertTrue( $response_data['active'] );
        return $card;
    }

    /**
     * Test Change CardList status on Disable by user, who created them
     * @test
     * @testdox Change CardList status on Disable by user, who created them
     * @depends getCardListOfUserAuth
     * @param $card CardList
     * @return CardList
     */
    public function changeCardListStatusOnDisableByUserAuth(CardList $card): CardList
    {
        /** @var CardList $card */
        $card = self::$cards->random(1)->first();

        $this->get(
                $this->getUrl().'/'.$card->id.'/disable',
                $this->getHeaderForUser()
            )->assertStatus(200)
            ->assertJsonFragment($this->getResponseSuccess());

        $card->refresh();
        $this->assertFalse( $card->active );
        return $card;
    }

    /**
     * Test Getting CardList with status disable by user, who created them
     * @test
     * @testdox Getting CardList with status disable by user, who created them
     * @depends changeCardListStatusOnDisableByUserAuth
     * @param $card CardList
     * @return CardList
     */
    public function getDisableCardListOfUserAuth(CardList $card): CardList
    {
        $response_data = $this->get(
                $this->getUrl().'/'.$card->id,
                $this->getHeaderForUser()
            )->assertStatus(200)
            ->assertJsonFragment($this->getResponseSuccess())
            ->assertJsonStructure($this->getResponseStructureByModel())
            ->decodeResponseJson('data');

        $this->assertEquals( $card->id, $response_data['id']);
        $this->assertFalse( $response_data['active'] );
        return $card;
    }
    /**
     * Test Change CardList status on Active by user, who created them
     * @test
     * @testdox Change CardList status on Active by user, who created them
     * @depends changeCardListStatusOnDisableByUserAuth
     * @param $card CardList
     * @return CardList
     */
    public function changeCardListStatusOnActiveByUserAuth(CardList $card): CardList
    {
        $this->get(
                $this->getUrl().'/'.$card->id.'/activate',
                $this->getHeaderForUser()
            )->assertStatus(200)
            ->assertJsonFragment($this->getResponseSuccess());

        $card->refresh();
        $this->assertTrue( $card->active );
        return $card;
    }

    /**
     * Test Update CardList by user
     * @test
     * @testdox Update CardList tag by user
     * @depends changeCardListStatusOnActiveByUserAuth
     * @param $card CardList
     * @return CardList
     */
    public function updateCardListByUserAuth(CardList $card): CardList
    {
        $card_data = factory(CardList::class)->raw();
        $card_data['tags'] = [];
        $card_data['_method'] = 'PUT';
        $response_data = $this->post(
                $this->getUrl().'/'.$card->id,
                $card_data,
                $this->getHeaderForUser()
            )->assertStatus(200)
            ->assertJsonStructure($this->getResponseStructureByModel())
            ->assertJsonFragment($this->getResponseSuccess())
            ->decodeResponseJson('data');

        $this->assertNotEmpty( $response_data['id'] );
        $this->assertEquals( $card_data['name'], $response_data['name']);
        $this->assertCount( count($card_data['content']), $response_data['content']);
        $this->assertArraySubset( $card_data['content'], $response_data['content']);
        $this->assertEquals( $card_data['type'], $response_data['type']);
        $this->assertCount( 0, $response_data['tags']);
        $this->assertTrue( $response_data['active'] );

        $card = CardList::find( $response_data['id'] );
        $this->assertInstanceOf( CardList::class, $card);
        $this->assertEquals( $card->id, $response_data['id']);
        $this->assertEquals( $card->name, $response_data['name']);
        $this->assertCount( $card->content->count(), $response_data['content']);
        $this->assertArraySubset( $card->content->toArray(), $response_data['content']);
        $this->assertEquals( $card->type, $response_data['type']);
        $this->assertCount( 0, $response_data['tags']);
        $this->assertTrue( $response_data['active'] );

        return $card;
    }
    /**
     * Test Delete CardList by user, who created them
     *
     * @test
     * @testdox Delete CardList by user, who created them
     * @depends updateCardListByUserAuth
     * @param $card CardList
     * @return CardList
     */
    public function deleteCardListByUserAuth(CardList $card): CardList
    {
        $this->delete(
                $this->getUrl().'/'.$card->id,
                [],
                $this->getHeaderForUser()
            )->assertStatus(200)
            ->assertJsonFragment( $this->getResponseSuccess() );

        $card->refresh();
        $this->assertTrue( $card->trashed() );
        return $card;
    }

    /**
     * Test Getting deleted CardList by user, who created them
     * @test
     * @testdox Getting deleted CardList by user, who created them
     * @depends deleteCardListByUserAuth
     * @param $card CardList
     * @return void
     */
    public function getDeletedCardListOfUserAuth(CardList $card): void
    {
        $this->get(
                $this->getUrl().'/'.$card->id,
                $this->getHeaderForUser()
            )->assertStatus(404);
        $card->forceDelete();
    }

    /**
     * Test Getting CardList's list by user, who created them, after Deleted one card
     * @test
     * @testdox Getting CardList's list by user, who created them, after Deleted one card
     * @depends getDeletedCardListOfUserAuth
     * @return void
     */
    public function getCardListsOfUserAuth(): void
    {
        $this->get(
            $this->getUrl(),
            $this->getHeaderForUser()
        )->assertStatus(200)
            ->assertJsonFragment($this->getResponseSuccess())
            ->assertJsonStructure($this->getResponseStructureByModelList())
            ->assertJsonCount(4,'data');
    }
}
