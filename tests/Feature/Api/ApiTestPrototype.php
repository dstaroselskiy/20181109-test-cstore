<?php

namespace Tests\Feature\Api;

use App\Models\Card;
use App\Models\Tag;
use App\Models\User;
use Illuminate\Database\Eloquent\Collection;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

abstract class ApiTestPrototype extends TestCase
{
    /**
     * return header for request by user
     *
     * @return array
     */
    abstract function getHeaderForUser():array;

    /**
     * return url for test
     *
     * @return string
     */
    abstract public function getUrl(): string;

    /**
     * return response structure by card list
     *
     * @return array
     */
    public function getResponseStructureByModelList(): array
    {
        return [
            'data' => [
                '*' => [
                    'id',
                ]
            ],
            'success'
        ];
    }

    /**
     * return response structure by card info
     *
     * @return array
     */
    public function getResponseStructureByModel(): array
    {
        return [
            'data' => [
                'id',
            ],
            'success'
        ];
    }
    /**
     * return structure by success response
     *
     * @return array
     */
    public function getResponseSuccess(): array
    {
        return [
            'success' => true
        ];
    }
    /** Prepare data's for test
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->prepareDataForTest();
    }

    /**
     * Prepare data's for testing
     *
     * @return void
     */
    abstract public function prepareDataForTest(): void;

    /**
     * Delete Data's after test
     * @afterClass
     */
    abstract public static function deleteData(): void;
}
