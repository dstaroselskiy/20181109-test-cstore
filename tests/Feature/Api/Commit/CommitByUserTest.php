<?php

namespace Tests\Feature\Api\Commit;

use App\Models\Card;
use App\Models\Commit;
use App\Models\Tag;
use App\Models\User;
use Illuminate\Database\Eloquent\Collection;
use Tests\Feature\Api\ApiTestPrototype;
use JWTAuth;

/**
 * Test api request for getting card information by user, who is authorisated and created it
 *
 * @package Tests\Feature\Api\Commit
 */
class CommitByUserTest extends ApiTestPrototype
{
    use CommitResponseTrait;
    /** @var User */
    public static $user;
    /** @var string */
    public static $user_token;
    /** @var Card */
    public static $card;
    /** @var Collection */
    public static $commits;
    /**
     * Prepare data's for testing
     *
     * @return void
     */
    public function prepareDataForTest(): void
    {
        if( empty( self::$user ) ){
            /** @var Collection $users */
            self::$user = factory(User::class)->create();
            self::$user_token = \JWTAuth::fromUser(self::$user);
        }
        if( empty( self::$card ) ){
            /** @var User $user */
            $user = self::$user;
            /* Create tags for card */
            self::$card = factory(Card::class)->make();
            self::$card->user()->associate($user)->save();
        }
        if( empty( self::$commits ) ){
            /** @var User $user */
            $user = self::$user;
            /** @var Card $card */
            $card = self::$card;
            /* Create tags for card */
            self::$commits = factory(Commit::class, 4)->make()->each(function(Commit $commit)use($user, $card){
                $commit->user()->associate($user)
                    ->card()->associate($card)
                    ->save();
                return $commit;
            });
        }
    }

    /**
     * Delete Data's after test
     * @afterClass
     */
    public static function deleteData(): void
    {
        if( !empty( self::$user ) ){
            self::$user->forceDelete();
        }
        if( !empty( self::$card ) ){
            self::$card->forceDelete();
        }
        if( !empty( self::$commits ) ){
            self::$commits->each(function(Commit $commit){$commit->forceDelete();});
        }
    }

    /**
     * return header for request by user
     *
     * @return array
     */
    public function getHeaderForUser():array
    {
        return [
            'Accept' => 'application/json',
            'Authorization' => 'Bearer '.self::$user_token,
        ];
    }


    /**
     * Test Getting commits card list by user, who created them
     *
     * @test
     * @testdox Getting  commits cards list by user, who created them
     * @return void
     */
    public function getListOfModels(): void
    {
        $this->get(
            $this->getUrl(),
            $this->getHeaderForUser()
        )->assertStatus(200)
            ->assertJsonFragment($this->getResponseSuccess())
            ->assertJsonStructure($this->getResponseStructureByModelList())
            ->assertJsonCount(4,'data');
    }

    /**
     * Test Storing new commit for card by user
     * @test
     * @testdox Storing new commit for card by user
     * @return Commit
     */
    public function storeCommitByUserAuth(): Commit
    {
        $commit_data = factory(Commit::class)->raw();
        $commit_data['user'] = self::$user->toArray();
        $response_data = $this->post(
                $this->getUrl(),
            $commit_data,
                $this->getHeaderForUser()
            )->assertStatus(201)
            ->assertJsonStructure($this->getResponseStructureByModel())
            ->assertJsonFragment($this->getResponseSuccess())
            ->decodeResponseJson('data');

        $this->assertNotEmpty( $response_data['id'] );
        $this->assertEquals( $commit_data['content'], $response_data['content']);
        $this->assertEquals( self::$user->id, $response_data['user']['id']);
        $this->assertEquals( self::$user->name, $response_data['user']['name']);

        $commit = Commit::find( $response_data['id'] );
        $this->assertInstanceOf( Commit::class, $commit);
        $this->assertEquals( $commit->id, $response_data['id']);
        $this->assertEquals( $commit->content, $response_data['content']);
        $this->assertEquals( $commit->user_id, $response_data['user']['id']);

        return $commit;
    }
    /**
     * Test Getting cards commit by user, who created them
     * @test
     * @testdox Getting cards commit by user, who created them
     * @depends storeCommitByUserAuth
     * @param $commit Commit
     * @return Commit
     */
    public function getCommitOfUserAuth(Commit $commit): Commit
    {
        $response_data = $this->get(
                $this->getUrl().'/'.$commit->id,
                $this->getHeaderForUser()
            )->assertStatus(200)
            ->assertJsonFragment($this->getResponseSuccess())
            ->assertJsonStructure($this->getResponseStructureByModel())
            ->decodeResponseJson('data');

        $this->assertEquals( $commit->id, $response_data['id']);
        $this->assertEquals( $commit->content, $response_data['content']);
        $this->assertEquals( $commit->user_id, $response_data['user']['id']);
        return $commit;
    }

    /**
     * Test Update the cards commit by user
     *
     * @test
     * @testdox Update the cards commit by user
     * @depends getCommitOfUserAuth
     * @param Commit $commit
     * @return Commit
     */
    public function updateCommitByAuthUser(Commit $commit): Commit
    {
        $card_data = factory(Commit::class)->raw();
        $card_data['_method'] = 'PUT';
        $response_data = $this->post(
                $this->getUrl().'/'.$commit->id,
                $card_data,
                $this->getHeaderForUser()
            )->assertStatus(200)
            ->assertJsonStructure($this->getResponseStructureByModel())
            ->assertJsonFragment($this->getResponseSuccess())
            ->decodeResponseJson('data');

        $this->assertNotEmpty( $response_data['id'] );
        $this->assertEquals( $card_data['content'], $response_data['content']);
        $this->assertEquals( self::$user->id, $response_data['user']['id']);
        $this->assertEquals( self::$user->name, $response_data['user']['name']);

        $commit = Commit::with(['user'])->find( $response_data['id'] );
        $this->assertInstanceOf( Commit::class, $commit);
        $this->assertEquals( $commit->id, $response_data['id']);
        $this->assertEquals( $commit->content, $response_data['content']);
        $this->assertEquals( $commit->user->id, $response_data['user']['id']);
        $this->assertEquals( $commit->user->name, $response_data['user']['name']);

        return $commit;
    }
    /**
     * Test Delete cards commit by user, who created them
     *
     * @test
     * @testdox Delete cards commit by user, who created them
     * @depends updateCommitByAuthUser
     * @param $commit Commit
     * @return Commit
     */
    public function deleteCommitByUserAuth(Commit $commit): Commit
    {
        $this->delete(
                $this->getUrl().'/'.$commit->id,
                [],
                $this->getHeaderForUser()
            )->assertStatus(200)
            ->assertJsonFragment( $this->getResponseSuccess() );

        $commit->refresh();
        $this->assertTrue( $commit->trashed() );
        return $commit;
    }

    /**
     * Test Getting deleted cards commit by user, who created them
     * @test
     * @testdox Getting deleted cards commit by user, who created them
     * @depends deleteCommitByUserAuth
     * @param Commit $commit
     * @return void
     */
    public function getDeletedCardOfUserAuth(Commit $commit): void
    {
        $this->get(
                $this->getUrl().'/'.$commit->id,
                $this->getHeaderForUser()
            )->assertStatus(404);
        $commit->forceDelete();
    }
}
