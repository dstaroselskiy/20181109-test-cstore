<?php

namespace Tests\Feature\Api\Commit;

/**
 * Prototype for test api request for getting commit information for card
 *
 * @package Tests\Feature\Api\Commit
 */
trait CommitResponseTrait
{
    /**
     * return url for test
     *
     * @return string
     */
    public function getUrl(): string
    {
        return 'api/cards/'.static::$card->id.'/commits';
    }

    /**
     * return response structure by cards commit list
     *
     * @return array
     */
    public function getResponseStructureByModelList(): array
    {
        return [
            'data' => [
                '*' => [
                    'id',
                    'user' => [
                        'id',
                        'name'
                    ],
                    'content',
                    'created_at',
                ]
            ],
            'success'
        ];
    }

    /**
     * return response structure by cards commit info
     *
     * @return array
     */
    public function getResponseStructureByModel(): array
    {
        return [
            'data' => [
                'id',
                'user' => [
                    'id',
                    'name'
                ],
                'content',
                'created_at',
            ],
            'success'
        ];
    }



}
