<?php

namespace Tests\Unit\Models;

use App\Models\Card;
use App\Models\Commit;
use App\Models\Tag;
use App\Models\User;
use Faker\Factory;
use Illuminate\Database\QueryException;
use Illuminate\Support\Collection;
use PDOException;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CommitTest extends TestCase
{
    /** @var User */
    public static $user;
    /** @var Card */
    public static $card;

    /** Prepare data for test - User,Card,
     */
    public function setUp()
    {
        parent::setUp();
        if( empty( self::$user ) ){
            $user_data = factory(User::class)->raw();
            /* Create new */
            self::$user = User::create([
                'name' => $user_data['name'],
                'email' => $user_data['email'],
                'password' => $user_data['password'],
            ]);
        }
        if( empty( self::$card ) ){
            $card_data = factory(Card::class)->raw();
            /* Create new */
            self::$card = (new Card() )
                ->fill($card_data);
            self::$card->user()->associate( self::$user )->save();
        }
    }

    /**
     * Delete data after test
     * @afterClass
     */
    public static function deleteUser()
    {
        if( !empty( self::$user ) ){
            self::$user->forceDelete();
        }
        if( !empty( self::$card ) ){
            self::$card->forceDelete();
        }
    }

    /**
     * Check creating Commit by card.
     * @test
     * @testdox Check Cards commit creation
     * @return Commit
     *
     */
    public function createCommitModel(): Commit
    {
        /*Check is User created for test*/
        $this->assertInstanceOf(User::class, self::$user);
        /*Check is Card created for test*/
        $this->assertInstanceOf(Card::class, self::$card);

        $commit_data = factory(Commit::class)->raw();
        /* Create new */
        /** @var Commit $commit */
        $commit = ( new Commit() )
            ->fill($commit_data)
            ->card()->associate( self::$card )
            ->user()->associate( self::$user );
        $commit->save();

        $this->assertEquals($commit_data['content'], $commit->content);
        $this->assertEquals(self::$user->id, $commit->user_id);
        $this->assertEquals(self::$card->id, $commit->card_id);
        return $commit;
    }

    /**
     * Check finding Cards Commit by id from DB.
     * @test
     * @testdox Check finding Cards Commit by id from DB
     * @depends createCommitModel
     * @param Commit $commit
     * @return Commit
     *
     */
    public function findcommitModelById(Commit $commit): Commit
    {
        /* Load card data's from DB */
        /** @var Commit $find_commit */
        $find_commit = Commit::find( $commit->id );
        $this->assertInstanceOf(Commit::class, $find_commit);

        $this->assertEquals($commit->id, $find_commit->id);
        $this->assertEquals($commit->content, $find_commit->content);
        $this->assertEquals(self::$user->id, $find_commit->user_id);
        $this->assertEquals(self::$card->id, $find_commit->card_id);
        return $find_commit;
    }

    /**
     * Check update the Commit by card.
     * @test
     * @testdox Check update the CaCommit by cardrd
     * @depends findcommitModelById
     * @param Commit $commit
     * @return Commit
     *
     */
    public function updateCommitModel(Commit $commit): Commit
    {
        $commit_data = factory(Commit::class)->raw();
        $commit->fill($commit_data)->save();

        $this->assertEquals($commit_data['content'], $commit->content);
        $this->assertEquals(self::$user->id, $commit->user_id);
        $this->assertEquals(self::$card->id, $commit->card_id);

        /* Load card data's from DB */
        /** @var Commit $find_commit */
        $find_commit = Commit::find( $commit->id );
        $this->assertInstanceOf(Commit::class, $find_commit);

        $this->assertEquals($commit_data['content'], $find_commit->content);
        $this->assertEquals(self::$user->id, $find_commit->user_id);
        $this->assertEquals(self::$card->id, $find_commit->card_id);
        return $find_commit;
    }

    /**
     * Check soft deleting the cards commit.
     * @test
     * @testdox Check soft deleting the cards commit.
     * @depends updateCommitModel
     * @param Commit $commit
     * @return Commit
     *
     * @throws \Exception
     */
    public function softDeleteCommitModel(Commit $commit): Commit
    {
        $commit->delete();
        /* Load card data's from DB */
        /** @var Commit $delete_commit */
        $delete_commit = Commit::find( $commit->id );
        $this->assertNotInstanceOf(Commit::class, $delete_commit);

        /* Find object in trash */
        /* Load card data's from DB */
        /** @var Commit $delete_commit */
        $delete_commit = Commit::onlyTrashed()->find( $commit->id );
        $this->assertInstanceOf(Commit::class, $delete_commit);

        $this->assertEquals($commit->content, $delete_commit->content);
        $this->assertEquals(self::$user->id, $delete_commit->user_id);
        $this->assertEquals(self::$card->id, $delete_commit->card_id);

        return $delete_commit;
    }

    /**
     * Check Restoring by deleted Cards Commit.
     * @test
     * @testdox Check Restoring by deleted Cards Commit
     * @depends softDeleteCommitModel
     * @param Commit $commit
     * @return Commit
     *
     * @throws \Exception
     */
    public function restoreCommitModel(Commit $commit): Commit
    {
        $commit->restore();
        /* Load card data's from DB */
        /** @var Commit $restored_commit */
        $restored_commit = Commit::find( $commit->id );
        $this->assertInstanceOf(Commit::class, $restored_commit);

        $this->assertEquals($commit->content, $restored_commit->content);
        $this->assertEquals(self::$user->id, $restored_commit->user_id);
        $this->assertEquals(self::$card->id, $restored_commit->card_id);

        return $commit;
    }

    /**
     * Check Cards Commmit force delete.
     * @test
     * @testdox Check Cards Commmit force delete
     * @depends restoreCommitModel
     * @param Commit $commit
     *
     * @throws \Exception
     */
    public function forceDeleteCardModel(Commit $commit)
    {
        $commit->forceDelete();
        /* Load card data's from DB */
        $delete_commit = Commit::withTrashed()->find( $commit->id );
        $this->assertNotInstanceOf(Commit::class, $delete_commit);
    }

    /**
     * Check Cascade Cards Commit deletion after delete user.
     * @test
     * @testdox Check Cascade Cards Commit deletion after delete user
     *
     * @throws \Exception
     */
    public function cascadeDeleteCommitModelByUser()
    {
        $user_data = factory(User::class)->raw();
        /* Create new */
        /** @var User $user */
        $user = User::create([
            'name' => $user_data['name'],
            'email' => $user_data['email'],
            'password' => $user_data['password'],
        ]);
        $card_data = factory(Card::class)->raw();
        /* Create new */
        /** @var Card $card */
        $card = (new Card() )
            ->fill($card_data)
            ->user()
            ->associate( self::$user );
        $card->save();

        $commit_data = factory(Commit::class)->raw();
        /* Create new */
        /** @var Commit $commit */
        $commit = ( new Commit() )
            ->fill($commit_data)
            ->card()->associate( $card )
            ->user()->associate( $user );
        $commit->save();

        $this->assertNotEmpty( $commit->id );

        $user->forceDelete();

        /* Load cards commit data's from DB */
        $delete_commit = Commit::withTrashed()->find( $commit->id );
        $this->assertNotInstanceOf(Commit::class, $delete_commit);
    }

    /**
     * Check Cascade Cards Commit deletion after delete card.
     * @test
     * @testdox Check Cascade Cards Commit deletion after delete card
     *
     * @throws \Exception
     */
    public function cascadeDeleteCommitModelByCard()
    {
        $card_data = factory(Card::class)->raw();
        /* Create new */
        /** @var Card $card */
        $card = (new Card() )
            ->fill($card_data)
            ->user()
            ->associate( self::$user );
        $card->save();

        $commit_data = factory(Commit::class)->raw();
        /* Create new */
        /** @var Commit $commit */
        $commit = ( new Commit() )
            ->fill($commit_data)
            ->card()->associate( $card )
            ->user()->associate( self::$user );
        $commit->save();

        $this->assertNotEmpty( $commit->id );

        $card->forceDelete();

        /* Load cards commit data's from DB */
        $delete_commit = Commit::withTrashed()->find( $commit->id );
        $this->assertNotInstanceOf(Commit::class, $delete_commit);
    }


}
