<?php

namespace Tests\Unit\Models;

use App\Models\User;
use Illuminate\Database\QueryException;
use PDOException;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserTest extends TestCase
{
    /**
     * Check Exception on creating User with out name.
     * @test
     * @testdox Check Exception on creating User with out name (can't create object)
     * @expectedException PDOException
     * @expectedExceptionCode HY000
     * @return void
     */
    public function itThrowsExceptionWhileGettingNonExistentItemByName(): void
    {
        $user_data = factory(User::class)->raw();
        /* Create new */
        User::create([
            'email' => $user_data['email'],
            'password' => $user_data['password'],
        ]);
    }

    /**
     * Check Exception on creating User with out email.
     * @test
     * @testdox Check Exception on creating User with out email (can't create object)
     * @expectedException PDOException
     * @expectedExceptionCode HY000
     * @return void
     *
     */
    public function itThrowsExceptionWhileGettingNonExistentItemByEmail(): void
    {
        $user_data = factory(User::class)->raw();
        /* Create new */
        User::create([
            'name' => $user_data['name'],
            'password' => $user_data['password'],
        ]);
    }
    /**
     * Check creating User.
     * @test
     * @testdox Check User creation
     * @return User
     *
     */
    public function createUserModel(): User
    {
        $user_data = factory(User::class)->raw();
        /* Create new */
        /** @var User $user */
        $user = User::create([
                'name' => $user_data['name'],
                'email' => $user_data['email'],
                'password' => $user_data['password'],
        ]);

        $this->assertEquals($user_data['name'], $user->name);
        $this->assertEquals($user_data['email'], $user->email);
        $this->assertNotEquals($user_data['password'], $user->password);
        return $user;
    }

    /**
     * Check finding User by id from DB.
     * @test
     * @testdox Check finding User by id from DB
     * @depends createUserModel
     * @param User $user
     * @return User
     *
     */
    public function findUserModelById(User $user): User
    {
        /* Load user data's from DB */
        /** @var User $find_user */
        $find_user = User::find( $user->id );
        $this->assertInstanceOf(User::class, $find_user);

        $this->assertEquals($user->name, $find_user->name);
        $this->assertEquals($user->email, $find_user->email);
        $this->assertEquals($user->password, $find_user->password);
        return $user;
    }

    /**
     * Check finding User by email from DB.
     * @test
     * @testdox Check finding User by email from DB
     * @depends findUserModelById
     * @param User $user
     * @return User
     *
     */
    public function findUserModelByEmail(User $user): User
    {
        /* Load user data's from DB */
        /** @var User $find_user */
        $find_user = User::where('email', $user->email )->first();
        $this->assertInstanceOf(User::class, $find_user);

        $this->assertEquals($user->id, $find_user->id);
        $this->assertEquals($user->name, $find_user->name);
        $this->assertEquals($user->password, $find_user->password);
        return $user;
    }

    /**
     * Check soft deleting by User.
     * @test
     * @testdox Check soft deleting by User
     * @depends findUserModelByEmail
     * @param User $user
     * @return User
     *
     * @throws \Exception
     */
    public function softDeleteUserModel(User $user): User
    {
        $user->delete();
        /* Load user data's from DB */
        /** @var User $delete_user */
        $delete_user = User::find( $user->id );
        $this->assertNotInstanceOf(User::class, $delete_user);

        /* Find object in trash */
        /* Load user data's from DB */
        $delete_user = User::onlyTrashed()->find( $user->id );
        $this->assertInstanceOf(User::class, $delete_user);

        $this->assertEquals($user->name, $delete_user->name);
        $this->assertEquals($user->email, $delete_user->email);
        $this->assertEquals($user->password, $delete_user->password);
        return $user;
    }

    /**
     * Check Restoring by deleted User.
     * @test
     * @testdox Check Restoring by deleted User
     * @depends softDeleteUserModel
     * @param User $user
     * @return User
     *
     * @throws \Exception
     */
    public function restoreUserModel(User $user): User
    {
        $user->restore();
        /* Load user data's from DB */
        /** @var User $restored_user */
        $restored_user = User::find( $user->id );
        $this->assertInstanceOf(User::class, $restored_user);

        $this->assertEquals($user->name, $restored_user->name);
        $this->assertEquals($user->email, $restored_user->email);
        $this->assertEquals($user->password, $restored_user->password);
        return $user;
    }

    /**
     * Check User force delete.
     * @test
     * @testdox Check User force delete
     * @depends restoreUserModel
     * @param User $user
     *
     * @throws \Exception
     */
    public function forceDeleteUserModel(User $user)
    {
        $user->forceDelete();
        /* Load user data's from DB */
        $delete_user = User::withTrashed()->find( $user->id );
        $this->assertNotInstanceOf(User::class, $delete_user);
    }


}
