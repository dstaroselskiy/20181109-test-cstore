<?php

namespace Tests\Unit\Models;

use App\Models\Card;
use App\Models\CardList;
use App\Models\Tag;
use App\Models\User;
use Faker\Factory;
use Illuminate\Database\QueryException;
use Illuminate\Support\Collection;
use PDOException;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CardListListTest extends TestCase
{
    /** @var User */
    public static $user;
    /** @var Collection */
    public static $tags;

    /** Create User for CardList
     */
    public function setUp()
    {
        parent::setUp();
        if( empty( self::$user ) ){
            $user_data = factory(User::class)->raw();
            /* Create new */
            self::$user = User::create([
                'name' => $user_data['name'],
                'email' => $user_data['email'],
                'password' => $user_data['password'],
            ]);
        }
        if( empty( self::$tags ) ){
            /* Create tags for card */
            /** @var Collection $tags_data */
            self::$tags = factory(Tag::class, 4)->create();
        }
    }

    /**
     * Delete User after test
     * @afterClass
     */
    public static function deleteUser()
    {
        if( !empty( self::$user ) ){
            self::$user->forceDelete();
        }
        if( !empty( self::$tags ) ){
            self::$tags->each(function(Tag $tag){$tag->forceDelete();});
        }
    }

    /**
     * Check creating CardList.
     * @test
     * @testdox Check CardList creation
     * @return CardList
     *
     */
    public function createCardListModel(): CardList
    {
        /*Check is User created for test*/
        $this->assertInstanceOf(User::class, self::$user);

        $card_data = factory(CardList::class)->raw();
        /* Create new */
        /** @var CardList $card */
        $card = ( new CardList() )->fill($card_data);
        $card->user()->associate( self::$user );
        $card->save();

        $this->assertEquals($card_data['name'], $card->name);
        $this->assertCount(count($card_data['content']), $card->content);
        $this->assertArraySubset( $card_data['content'], $card->content->toArray());
        $this->assertEquals(self::$user->id, $card->user_id);
        return $card;
    }

    /**
     * Check finding CardList by id from DB.
     * @test
     * @testdox Check finding CardList by id from DB
     * @depends createCardListModel
     * @param CardList $card
     * @return CardList
     *
     */
    public function findCardListModelById(CardList $card): CardList
    {
        /* Load card data's from DB */
        /** @var CardList $find_card */
        $find_card = CardList::find( $card->id );
        $this->assertInstanceOf(CardList::class, $find_card);

        $this->assertEquals($card->name, $find_card->name);
        $this->assertCount(count($card->content), $find_card->content);
        $this->assertArraySubset( $card->content->toArray(), $find_card->content->toArray());
        $this->assertEquals(self::$user->id, $find_card->user_id);
        return $card;
    }

    /**
     * Check update the CardList.
     * @test
     * @testdox Check update the CardList
     * @depends findCardListModelById
     * @param CardList $card
     * @return CardList
     *
     */
    public function updateCardListModel(CardList $card): CardList
    {
        $card_data = factory(CardList::class)->raw();
        $card->fill($card_data)->save();

        $this->assertEquals($card_data['name'], $card->name);
        $this->assertCount(count($card_data['content']), $card->content);
        $this->assertArraySubset($card_data['content'], $card->content->toArray());
        $this->assertEquals(self::$user->id, $card->user_id);

        /* Load card data's from DB */
        /** @var CardList $find_card */
        $find_card = CardList::find( $card->id );
        $this->assertInstanceOf(CardList::class, $find_card);

        $this->assertEquals($card->name, $find_card->name);
        $this->assertCount(count($card->content), $find_card->content);
        $this->assertArraySubset( $card->content->toArray(), $find_card->content->toArray());
        $this->assertEquals(self::$user->id, $find_card->user_id);
        return $card;
    }

    /**
     * Check User relation by CardList.
     * @test
     * @testdox Check User relation by CardList
     * @depends updateCardListModel
     * @param CardList $card
     * @return CardList
     *
     */
    public function relationCardListAndUser(CardList $card): CardList
    {
        /* Load card data's from DB */
        $find_card = CardList::with('user')->find( $card->id );
        $this->assertInstanceOf(CardList::class, $find_card);
        $this->assertInstanceOf(User::class, $find_card->user);

        $this->assertEquals(self::$user->id, $find_card->user->id);
        return $card;
    }

    /**
     * Check soft deleting by CardList.
     * @test
     * @testdox Check soft deleting by CardList
     * @depends relationCardListAndUser
     * @param CardList $card
     * @return CardList
     *
     * @throws \Exception
     */
    public function softDeleteCardListModel(CardList $card): CardList
    {
        $card->delete();
        /* Load card data's from DB */
        /** @var CardList $delete_card */
        $delete_card = CardList::find( $card->id );
        $this->assertNotInstanceOf(CardList::class, $delete_card);

        /* Find object in trash */
        /* Load card data's from DB */
        /** @var CardList $delete_card */
        $delete_card = CardList::onlyTrashed()->find( $card->id );
        $this->assertInstanceOf(CardList::class, $delete_card);

        $this->assertEquals($card->name, $delete_card->name);
        $this->assertCount(count($card->content), $delete_card->content);
        $this->assertArraySubset( $card->content->toArray(), $delete_card->content->toArray());
        $this->assertEquals(self::$user->id, $delete_card->user_id);

        return $card;
    }

    /**
     * Check Restoring by deleted CardList.
     * @test
     * @testdox Check Restoring by deleted CardList
     * @depends softDeleteCardListModel
     * @param CardList $card
     * @return CardList
     *
     * @throws \Exception
     */
    public function restoreCardListModel(CardList $card): CardList
    {
        $card->restore();
        /* Load card data's from DB */
        /** @var CardList $restored_card */
        $restored_card = CardList::find( $card->id );
        $this->assertInstanceOf(CardList::class, $restored_card);

        $this->assertEquals($card->name, $restored_card->name);
        $this->assertCount(count($card->content), $restored_card->content);
        $this->assertArraySubset( $card->content->toArray(), $restored_card->content->toArray());
        $this->assertEquals(self::$user->id, $restored_card->user_id);

        return $card;
    }

    /**
     * Check CardList force delete.
     * @test
     * @testdox Check CardList force delete
     * @depends restoreCardListModel
     * @param CardList $card
     *
     * @throws \Exception
     */
    public function forceDeleteCardListModel(CardList $card)
    {
        $card->forceDelete();
        /* Load card data's from DB */
        $delete_card = CardList::withTrashed()->find( $card->id );
        $this->assertNotInstanceOf(CardList::class, $delete_card);
    }

    /**
     * Check CardList relation with Tags - sync CardList with tags.
     * @test
     * @testdox Check CardList relation with Tags - sync CardList with tags.
     * @return CardList
     * @throws \Exception
     */
    public function relationCardListAndTagsSync(): CardList
    {
        /* Check is User created for test*/
        $this->assertInstanceOf(User::class, self::$user);

        /* Check are Tags created for test*/
        $this->assertInstanceOf(Collection::class, self::$tags);
        $this->assertFalse(self::$tags->isEmpty());

        $card_data = factory(CardList::class)->raw();
        /* Create new */
        /** @var CardList $card */
        $card = (new CardList())->fill($card_data);
        $card->user()->associate(self::$user);
        $card->save();

        $this->assertNotEmpty($card->id);

        $card->tags()->sync(self::$tags);

        /** @var CardList $upload_card */
        $upload_card = CardList::with('tags')->find($card->id);
        //$card->tags()->sync(self::$tags->map(function(Tag $tag){return $tag->id;}));

        $this->assertEquals($card->name, $upload_card->name);
        $this->assertCount( $card->content->count(), $upload_card->content);
        $this->assertArraySubset( $card->content->toArray(), $upload_card->content->toArray());
        $this->assertEquals(self::$user->id, $upload_card->user_id);

        $this->assertCount(self::$tags->count(), $upload_card->tags);
        return $upload_card;
    }

    /**
     * Check CardList relation with Tags - Update sync CardList with Tags.
     * @test
     * @testdox Check CardList relation with Tags - Update sync CardList with Tags.
     * @depends relationCardListAndTagsSync
     * @param CardList $card
     * @return CardList
     * @throws \Exception
     */
    public function relationCardListAndTagsSyncUpdate(CardList $card): CardList
    {
        /** @var Collection $less_tags */
        $less_tags = self::$tags->random(2);

        $card->tags()->sync($less_tags);

        /** @var CardList $upload_card */
        $upload_card = CardList::with('tags')->find($card->id);
        //$card->tags()->sync(self::$tags->map(function(Tag $tag){return $tag->id;}));

        $this->assertEquals($card->name, $upload_card->name);
        $this->assertCount( $card->content->count(), $upload_card->content);
        $this->assertArraySubset( $card->content->toArray(), $upload_card->content->toArray());
        $this->assertEquals(self::$user->id, $upload_card->user_id);

        $this->assertCount($less_tags->count(), $upload_card->tags);
        return $upload_card;
    }

    /**
     * Check CardList relation with Tags - on CardList soft deletion.
     * @test
     * @testdox Check CardList relation with Tags - on CardList soft deletion.
     * @depends relationCardListAndTagsSyncUpdate
     * @param CardList $card
     * @throws \Exception
     */
    public function relationCardListAndTagsSyncDeletion(CardList $card): void
    {
        /* Delete CardList after test */
        $card->forceDelete();
        /* Load card data's from DB */
        $delete_card = CardList::withTrashed()->find( $card->id );
        $this->assertNotInstanceOf(CardList::class, $delete_card);

        /* Check tags list after card force deletion  */
        foreach ( self::$tags as $tag ) {
            /* Load card data's from DB */
            $load_tag = Tag::find( $tag->id );
            $this->assertInstanceOf(Tag::class, $load_tag);
        }
    }

    /**
     * Check Cascade CardList deletion after delete user.
     * @test
     * @testdox Check Cascade CardList deletion after delete user
     *
     * @throws \Exception
     */
    public function cascadeDeleteCardListModel()
    {
        /*Check is User created for test*/
        $this->assertInstanceOf(User::class, self::$user);

        $card_data = factory(CardList::class)->raw();
        /* Create new */
        /** @var CardList $card */
        $card = ( new CardList() )->fill($card_data);
        $card->user()->associate( self::$user );
        $card->save();

        $this->assertNotEmpty( $card->id );

        self::$user->forceDelete();

        /* Load card data's from DB */
        $delete_card = CardList::withTrashed()->find( $card->id );
        $this->assertNotInstanceOf(CardList::class, $delete_card);
    }


}
