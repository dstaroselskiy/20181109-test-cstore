<?php

namespace Tests\Unit\Models;

use App\Models\Card;
use App\Models\Tag;
use App\Models\User;
use Faker\Factory;
use Illuminate\Database\QueryException;
use Illuminate\Support\Collection;
use PDOException;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CardTest extends TestCase
{
    /** @var User */
    public static $user;
    /** @var Collection */
    public static $tags;

    /** Create User for Card
     */
    public function setUp()
    {
        parent::setUp();
        if( empty( self::$user ) ){
            $user_data = factory(User::class)->raw();
            /* Create new */
            self::$user = User::create([
                'name' => $user_data['name'],
                'email' => $user_data['email'],
                'password' => $user_data['password'],
            ]);
        }
        if( empty( self::$tags ) ){
            /* Create tags for card */
            /** @var Collection $tags_data */
            self::$tags = factory(Tag::class, 4)->create();
        }
    }

    /**
     * Delete User after test
     * @afterClass
     */
    public static function deleteUser()
    {
        if( !empty( self::$user ) ){
            self::$user->forceDelete();
        }
        if( !empty( self::$tags ) ){
            self::$tags->each(function(Tag $tag){$tag->forceDelete();});
        }
    }

    /**
     * Check creating Card.
     * @test
     * @testdox Check Card creation
     * @return Card
     *
     */
    public function createCardModel(): Card
    {
        /*Check is User created for test*/
        $this->assertInstanceOf(User::class, self::$user);

        $card_data = factory(Card::class)->raw();
        /* Create new */
        /** @var Card $card */
        $card = ( new Card() )->fill($card_data);
        $card->user()->associate( self::$user );
        $card->save();

        $this->assertEquals($card_data['name'], $card->name);
        $this->assertEquals($card_data['content'], $card->content);
        $this->assertEquals(self::$user->id, $card->user_id);
        return $card;
    }

    /**
     * Check finding Card by id from DB.
     * @test
     * @testdox Check finding Card by id from DB
     * @depends createCardModel
     * @param Card $card
     * @return Card
     *
     */
    public function findCardModelById(Card $card): Card
    {
        /* Load card data's from DB */
        /** @var Card $find_card */
        $find_card = Card::find( $card->id );
        $this->assertInstanceOf(Card::class, $find_card);

        $this->assertEquals($card->name, $find_card->name);
        $this->assertEquals($card->content, $find_card->content);
        $this->assertEquals(self::$user->id, $find_card->user_id);
        return $card;
    }

    /**
     * Check update the Card.
     * @test
     * @testdox Check update the Card
     * @depends findCardModelById
     * @param Card $card
     * @return Card
     *
     */
    public function updateCardModel(Card $card): Card
    {
        $card_data = factory(Card::class)->raw();
        $card->fill($card_data)->save();

        $this->assertEquals($card_data['name'], $card->name);
        $this->assertEquals($card_data['content'], $card->content);
        $this->assertEquals(self::$user->id, $card->user_id);

        /* Load card data's from DB */
        /** @var Card $find_card */
        $find_card = Card::find( $card->id );
        $this->assertInstanceOf(Card::class, $find_card);

        $this->assertEquals($card->name, $find_card->name);
        $this->assertEquals($card->content, $find_card->content);
        $this->assertEquals(self::$user->id, $find_card->user_id);
        return $card;
    }

    /**
     * Check User relation by Card.
     * @test
     * @testdox Check User relation by Card
     * @depends updateCardModel
     * @param Card $card
     * @return Card
     *
     */
    public function relationCardAndUser(Card $card): Card
    {
        /* Load card data's from DB */
        $find_card = Card::with('user')->find( $card->id );
        $this->assertInstanceOf(Card::class, $find_card);
        $this->assertInstanceOf(User::class, $find_card->user);

        $this->assertEquals(self::$user->id, $find_card->user->id);
        return $card;
    }

    /**
     * Check soft deleting by Card.
     * @test
     * @testdox Check soft deleting by Card
     * @depends relationCardAndUser
     * @param Card $card
     * @return Card
     *
     * @throws \Exception
     */
    public function softDeleteCardModel(Card $card): Card
    {
        $card->delete();
        /* Load card data's from DB */
        /** @var Card $delete_card */
        $delete_card = Card::find( $card->id );
        $this->assertNotInstanceOf(Card::class, $delete_card);

        /* Find object in trash */
        /* Load card data's from DB */
        /** @var Card $delete_card */
        $delete_card = Card::onlyTrashed()->find( $card->id );
        $this->assertInstanceOf(Card::class, $delete_card);

        $this->assertEquals($card->name, $delete_card->name);
        $this->assertEquals($card->content, $delete_card->content);
        $this->assertEquals(self::$user->id, $delete_card->user_id);

        return $card;
    }

    /**
     * Check Restoring by deleted Card.
     * @test
     * @testdox Check Restoring by deleted Card
     * @depends softDeleteCardModel
     * @param Card $card
     * @return Card
     *
     * @throws \Exception
     */
    public function restoreCardModel(Card $card): Card
    {
        $card->restore();
        /* Load card data's from DB */
        /** @var Card $restored_card */
        $restored_card = Card::find( $card->id );
        $this->assertInstanceOf(Card::class, $restored_card);

        $this->assertEquals($card->name, $restored_card->name);
        $this->assertEquals($card->content, $restored_card->content);
        $this->assertEquals(self::$user->id, $restored_card->user_id);

        return $card;
    }

    /**
     * Check Card force delete.
     * @test
     * @testdox Check Card force delete
     * @depends restoreCardModel
     * @param Card $card
     *
     * @throws \Exception
     */
    public function forceDeleteCardModel(Card $card)
    {
        $card->forceDelete();
        /* Load card data's from DB */
        $delete_card = Card::withTrashed()->find( $card->id );
        $this->assertNotInstanceOf(Card::class, $delete_card);
    }

    /**
     * Check Card relation with Tags - sync Card with tags.
     * @test
     * @testdox Check Card relation with Tags - sync Card with tags.
     * @return Card
     * @throws \Exception
     */
    public function relationCardAndTagsSync(): Card
    {
        /* Check is User created for test*/
        $this->assertInstanceOf(User::class, self::$user);

        /* Check are Tags created for test*/
        $this->assertInstanceOf(Collection::class, self::$tags);
        $this->assertFalse(self::$tags->isEmpty());

        $card_data = factory(Card::class)->raw();
        /* Create new */
        /** @var Card $card */
        $card = (new Card())->fill($card_data);
        $card->user()->associate(self::$user);
        $card->save();

        $this->assertNotEmpty($card->id);

        $card->tags()->sync(self::$tags);

        /** @var Card $upload_card */
        $upload_card = Card::with('tags')->find($card->id);
        //$card->tags()->sync(self::$tags->map(function(Tag $tag){return $tag->id;}));

        $this->assertEquals($card->name, $upload_card->name);
        $this->assertEquals($card->content, $upload_card->content);
        $this->assertEquals(self::$user->id, $upload_card->user_id);

        $this->assertCount(self::$tags->count(), $upload_card->tags);
        return $upload_card;
    }

    /**
     * Check Card relation with Tags - Update sync Card with Tags.
     * @test
     * @testdox Check Card relation with Tags - Update sync Card with Tags.
     * @depends relationCardAndTagsSync
     * @param Card $card
     * @return Card
     * @throws \Exception
     */
    public function relationCardAndTagsSyncUpdate(Card $card): Card
    {
        /** @var Collection $less_tags */
        $less_tags = self::$tags->random(2);

        $card->tags()->sync($less_tags);

        /** @var Card $upload_card */
        $upload_card = Card::with('tags')->find($card->id);
        //$card->tags()->sync(self::$tags->map(function(Tag $tag){return $tag->id;}));

        $this->assertEquals($card->name, $upload_card->name);
        $this->assertEquals($card->content, $upload_card->content);
        $this->assertEquals(self::$user->id, $upload_card->user_id);

        $this->assertCount($less_tags->count(), $upload_card->tags);
        return $upload_card;
    }

    /**
     * Check Card relation with Tags - on Card soft deletion.
     * @test
     * @testdox Check Card relation with Tags - on Card soft deletion.
     * @depends relationCardAndTagsSyncUpdate
     * @param Card $card
     * @throws \Exception
     */
    public function relationCardAndTagsSyncDeletion(Card $card): void
    {
        /* Delete Card after test */
        $card->forceDelete();
        /* Load card data's from DB */
        $delete_card = Card::withTrashed()->find( $card->id );
        $this->assertNotInstanceOf(Card::class, $delete_card);

        /* Check tags list after card force deletion  */
        foreach ( self::$tags as $tag ) {
            /* Load card data's from DB */
            $load_tag = Tag::find( $tag->id );
            $this->assertInstanceOf(Tag::class, $load_tag);
        }
    }

    /**
     * Check Cascade Card deletion after delete user.
     * @test
     * @testdox Check Cascade Card deletion after delete user
     *
     * @throws \Exception
     */
    public function cascadeDeleteCardModel()
    {
        /*Check is User created for test*/
        $this->assertInstanceOf(User::class, self::$user);

        $card_data = factory(Card::class)->raw();
        /* Create new */
        /** @var Card $card */
        $card = ( new Card() )->fill($card_data);
        $card->user()->associate( self::$user );
        $card->save();

        $this->assertNotEmpty( $card->id );

        self::$user->forceDelete();

        /* Load card data's from DB */
        $delete_card = Card::withTrashed()->find( $card->id );
        $this->assertNotInstanceOf(Card::class, $delete_card);
    }


}
