<?php

namespace Tests\Unit\Models;

use App\Models\Card;
use App\Models\Tag;
use App\Models\User;
use Illuminate\Database\QueryException;
use PDOException;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class TagTest extends TestCase
{
    /**
     * Check Exception on creating Tag with out name.
     * @test
     * @testdox Check Exception on creating Tag with out name (can't create object)
     * @expectedException PDOException
     * @expectedExceptionCode HY000
     * @return void
     */
    public function itThrowsExceptionWhileGettingNonExistentItemByName(): void
    {
        /* Create new */
        Tag::create([]);
    }

    /**
     * Check creating Tag.
     * @test
     * @testdox Check Tag creation
     * @return Tag
     *
     */
    public function createTagModel(): Tag
    {
        $tag_data = factory(Tag::class)->raw();
        /* Create new */
        /** @var Tag $tag */
        $tag = Tag::create([
            'name' => $tag_data['name'],
        ]);

        $this->assertEquals($tag_data['name'], $tag->name);
        return $tag;
    }

    /**
     * Check Exception on creating Tag with same name.
     * @test
     * @testdox Check Exception on creating Tag with same name
     * @depends createTagModel
     * @return Tag
     *
     */
    public function createSameTagModel(Tag $tag): Tag
    {
        try {
            Tag::create([
                'name' => $tag->name,
            ]);
        }catch(\Exception $error) {
            $this->assertInstanceOf( QueryException::class, $error );
            $this->assertEquals( 23000, $error->getCode() );
        }
        return $tag;
    }

    /**
     * Check finding Tag by id from DB.
     * @test
     * @testdox Check finding Tag by id from DB
     * @depends createSameTagModel
     * @param Tag $tag
     * @return Tag
     *
     */
    public function findTagModelById(Tag $tag): Tag
    {
        /* Load tag data's from DB */
        /** @var Tag $find_tag */
        $find_tag = Tag::find( $tag->id );
        $this->assertInstanceOf(Tag::class, $find_tag);

        $this->assertEquals($tag->name, $find_tag->name);
        return $tag;
    }

    /**
     * Check finding Tag by name from DB.
     * @test
     * @testdox Check finding Tag by name from DB
     * @depends findTagModelById
     * @param Tag $tag
     * @return Tag
     *
     */
    public function findTagModelByName(Tag $tag): Tag
    {
        /* Load tag data's from DB */
        /** @var Tag $find_tag */
        $find_tag = Tag::where('name', $tag->name )->first();
        $this->assertInstanceOf(Tag::class, $find_tag);

        $this->assertEquals($tag->id, $find_tag->id);
        return $tag;
    }

    /**
     * Check soft deleting by Tag.
     * @test
     * @testdox Check soft deleting by Tag
     * @depends findTagModelByName
     * @param Tag $tag
     * @return Tag
     *
     * @throws \Exception
     */
    public function softDeleteTagModel(Tag $tag): Tag
    {
        $tag->delete();
        /* Load tag data's from DB */
        /** @var Tag $delete_tag */
        $delete_tag = Tag::find( $tag->id );
        $this->assertNotInstanceOf(Tag::class, $delete_tag);

        /* Find object in trash */
        /* Load tag data's from DB */
        $delete_tag = Tag::onlyTrashed()->find( $tag->id );
        $this->assertInstanceOf(Tag::class, $delete_tag);

        $this->assertEquals($tag->name, $delete_tag->name);
        return $tag;
    }

    /**
     * Check Restoring by deleted Tag.
     * @test
     * @testdox Check Restoring by deleted Tag
     * @depends softDeleteTagModel
     * @param Tag $tag
     * @return Tag
     *
     * @throws \Exception
     */
    public function restoreTagModel(Tag $tag): Tag
    {
        $tag->restore();
        /* Load tag data's from DB */
        /** @var Tag $restored_tag */
        $restored_tag = Tag::find( $tag->id );
        $this->assertInstanceOf(Tag::class, $restored_tag);

        $this->assertEquals($tag->name, $restored_tag->name);
        return $tag;
    }

    /**
     * Check Tag force delete.
     * @test
     * @testdox Check Tag force delete
     * @depends restoreTagModel
     * @param Tag $tag
     *
     * @throws \Exception
     */
    public function forceDeleteTagModel(Tag $tag)
    {
        $tag->forceDelete();
        /* Load tag data's from DB */
        /** @var Tag $delete_tag */
        $delete_tag = Tag::withTrashed()->find( $tag->id );
        $this->assertNotInstanceOf(Tag::class, $delete_tag);
    }


}
