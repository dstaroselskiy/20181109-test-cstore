<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Nanigans\SingleTableInheritance\SingleTableInheritanceTrait;


/**
 * Class Card
 *
 * @package App\Models
 * @property integer    $id
 * @property string     $name
 * @property string     $content
 * @property string     $type
 * @property boolean    $active
 * @property integer    $user_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @property array      $attributes
 * @property User       $user
 * @property Collection $commits
 * @property Collection $tags
 *
 * @author  Dmytro staroselskyi
 * @version 1.0.1
 */
class Card extends Model
{
    use SoftDeletes, SingleTableInheritanceTrait;

    protected $table = "cards";

    protected static $singleTableTypeField = 'type';

    protected static $singleTableSubclasses = [
        CardList::class
    ];

    protected static $singleTableType = 'text';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'content',
        'active'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'active' => 'boolean',
    ];
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'id',
        'user_id',
        'created_at',
        'updated_at',
        'deleted_at',
        'type'
    ];

    /** Get relation by User
     *
     * @return BelongsTo
     *
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(
            User::class,
            'user_id',
            'id'
        );
    }

    /** Get relation by commits
     *
     * @return HasMany
     */
    public function commits(): HasMany
    {
        return $this->hasMany(
            Commit::class,
            'card_id',
            'id'
        );
    }

    /** Get relation by tags
     *
     * @return BelongsToMany
     */
    public function tags(): BelongsToMany
    {
        return $this->belongsToMany(
            Tag::class,
            'card_tag',
            'card_id',
            'tag_id'
        );
    }
}
