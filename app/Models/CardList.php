<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Builder;
use Spatie\SchemalessAttributes\SchemalessAttributes;

/**
 * Class CardList
 * @property SchemalessAttributes  $content
 * @package App\Models
 */
class CardList extends Card
{
    protected static $singleTableType = 'checkbox';

    public $casts = [
        'active' => 'boolean',
        'content' => 'array',
    ];

    public function getContentAttribute(): SchemalessAttributes
    {
        return SchemalessAttributes::createForModel($this, 'content');
    }

    public function scopeWithContent(): Builder
    {
        return SchemalessAttributes::scopeWithSchemalessAttributes('content');
    }
}
