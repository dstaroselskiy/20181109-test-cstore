<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Commit;
use Illuminate\Auth\Access\HandlesAuthorization;

class CommitPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the commit.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Commit  $commit
     * @return mixed
     */
    public function view(User $user, Commit $commit)
    {
        return $user->id === $commit->user_id;
    }

    /**
     * Determine whether the user can create commits.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->id??false;
    }

    /**
     * Determine whether the user can update the commit.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Commit  $commit
     * @return mixed
     */
    public function update(User $user, Commit $commit)
    {
        return $user->id === $commit->user_id;
    }

    /**
     * Determine whether the user can delete the commit.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Commit  $commit
     * @return mixed
     */
    public function delete(User $user, Commit $commit)
    {
        return $user->id === $commit->user_id;
    }

    /**
     * Determine whether the user can restore the commit.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Commit  $commit
     * @return mixed
     */
    public function restore(User $user, Commit $commit)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the commit.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Commit  $commit
     * @return mixed
     */
    public function forceDelete(User $user, Commit $commit)
    {
        //
    }
}
