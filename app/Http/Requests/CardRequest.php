<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CardRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        /** @var array $rule */
        $rule = [
            'name' => [
                'nullable',
                'string',
                'max:255',
                Rule::unique('tags','name')->ignore( $this->route()->parameter('tag')->id??0 )
            ],
            'type' => [
                'required',
                'string',
                'max:10',
                Rule::in(['text','checkbox']),
            ],
            'content' => [
                'nullable',
                'string',
            ],
            'active' => [
                'nullable',
                'boolean',
            ],
            'tags.*.id' => [
                'nullable',
                'numeric',
            ],
            'tags.*.name' => [
                'required',
                'string',
                'max:255',
            ]
        ];
        if( $this->get('type', 'text') === 'checkbox' ) {
            $rule['content'] = [
                'nullable',
                'array',
            ];
            $rule['content.*.checked'] = [
                'required',
                'boolean',
            ];
            $rule['content.*.name'] = [
                'nullable',
                'string',
                'max:255',
            ];
        }
        return $rule;
    }
}
