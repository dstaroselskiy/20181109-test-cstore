<?php

namespace App\Http\Resources\Card;

use App\Http\Resources\Tag\TagResource;
use App\Models\Card;
use App\Models\CardList;
use Illuminate\Http\Resources\Json\JsonResource;

class CardResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var Card|CardList $this */
        return [
            'id'  => $this->id,
            'name'  => $this->name,
            'content'  => ( $this->type === 'checkbox' ) ? $this->content->toArray() : $this->content,
            'type'  => $this->type,
            'active'  => $this->active??false,
            'tags' => ( $this->tags && !$this->tags->isEmpty() ) ? TagResource::collection($this->tags) : [],
        ];
    }
}
