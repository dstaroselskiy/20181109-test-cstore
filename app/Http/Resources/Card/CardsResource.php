<?php

namespace App\Http\Resources\Card;

use Illuminate\Http\Resources\Json\ResourceCollection;

class CardsResource extends ResourceCollection
{

    /**
     * Transform the resource into a JSON array.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'data'  => CardResource::collection($this->collection)
        ];
    }
}
