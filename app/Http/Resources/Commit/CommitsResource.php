<?php

namespace App\Http\Resources\Commit;

use Illuminate\Http\Resources\Json\ResourceCollection;

class CommitsResource extends ResourceCollection
{

    /**
     * Transform the resource into a JSON array.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'data'  => CommitResource::collection($this->collection)
        ];
    }
}
