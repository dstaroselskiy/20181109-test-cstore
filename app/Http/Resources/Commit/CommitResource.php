<?php

namespace App\Http\Resources\Commit;

use App\Http\Resources\Tag\TagResource;
use App\Models\Commit;
use Illuminate\Http\Resources\Json\JsonResource;

class CommitResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var Commit $this */
        return [
            'id'  => $this->id,
            'content'  => $this->content,
            'user'  => [
                'id' => $this->user->id??0,
                'name'  => $this->user->name??'',
            ],
            'created_at' => $this->created_at->toDateTimeString(),
        ];
    }
}
