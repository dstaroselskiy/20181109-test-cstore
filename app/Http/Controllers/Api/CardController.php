<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\CardRequest;
use App\Http\Resources\Card\CardResource;
use App\Http\Resources\Card\CardsResource;
use App\Models\Card;
use App\Models\CardList;
use App\Models\Tag;
use Auth;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;


class CardController extends Controller
{
    /**
     * Display a listing of Cards.
     *
     * @param  Request  $request
     * @return CardsResource
     */
    public function index(Request $request): CardsResource
    {
        /** @var Builder $card */
        $card = Card::select(['*']);
        if( $request->has('tags') ){
            $tags = $request->input('tags',[]);
            $card->with([
                'tags' => function(BelongsToMany $query)use($tags){
                    $query->whereIn('id', $tags );
                }
            ]);
        }
        $card->where('user_id', Auth::user()->id);
        $presenter = new CardsResource( $card->get() );
        $presenter->additional(['success' => true]);
        return $presenter;
    }
    /**
     * Display the Card information.
     *
     * @param  Card $card
     * @return CardResource
     */
    public function show(Card $card): CardResource
    {
        $card->loadMissing([
           'tags'
        ]);
        $presenter = new CardResource( $card );
        $presenter->additional(['success' => true]);
        return $presenter;
    }
    /**
     * Store a newly created card in storage.
     *
     * @param  CardRequest  $request
     * @return JsonResource
     */
    public function store(CardRequest $request): JsonResource
    {
        /** @var Card $card */
        $card = (  ( $request->input('type', 'text') === 'checkbox' ) ? new CardList() : new Card() )
            ->fill($request->toArray());
        $card->user()->associate( Auth::user() );
        if( $card->save() ) {

            $presenter = new CardResource( $this->makeTagRelation( $card, $request->input('tags', []) ));
            $presenter->additional(['success' => true]);
            return $presenter;
        }
        return response()->json([
            'success' => false
        ]);
    }

    /**
     * Make Card relation with given Tags
     *
     * @param Card $card
     * @param array $tags
     * @return Card
     */
    public function makeTagRelation(Card $card, array $tags): Card
    {
        if( !empty( $tags ) ) {
            $card->tags()->sync(array_map(function (array $tag) {
                return $tag['id'];
            }, $tags));
        }else{
            $card->tags()->detach();
        }
        $card->load([
            'tags'
        ]);
        return $card;
    }
    /**
     * Set active status by Card on TRUE.
     *
     * @param  Card $card
     * @return JsonResponse
     */
    public function activate(Card $card): JsonResponse
    {
        $card->active = true;
        return response()->json([
            'success' => $card->save()
        ]);
    }
    /**
     * Set active status by Card on FALSE.
     *
     * @param  Card $card
     * @return JsonResponse
     */
    public function disable(Card $card): JsonResponse
    {
        $card->active = false;
        return response()->json([
            'success' => $card->save()
        ]);
    }
    /**
     * Update the card resource in storage.
     *
     * @param  CardRequest  $request
     * @param  Card $card
     * @return JsonResource
     */
    public function update(CardRequest $request, Card $card): JsonResource
    {
        $card->fill($request->toArray());
        if( $card->save() ) {
            $presenter = new CardResource(  $this->makeTagRelation( $card, $request->input('tags', []) ) );
            $presenter->additional(['success' => true]);
            return $presenter;
        }
        return response()->json([
            'success' => false
        ]);
    }
    /**
     * Remove the Card from storage.
     *
     * Make Card soft deletion - set time by record deleted
     *
     * @param  Card $card
     * @return JsonResponse
     * @throws \Exception
     */
    public function destroy(Card $card): JsonResponse
    {
        return response()->json([
            'success' => $card->delete()
        ]);
    }

}
