<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\TagRequest;
use App\Http\Resources\Tag\TagResource;
use App\Http\Resources\Tag\TagsResource;
use App\Models\Tag;
use Auth;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Resources\Json\JsonResource;

class TagController extends Controller
{
    /**
     * Display a listing of Tags.
     *
     * @param  Request  $request
     * @return TagsResource
     */
    public function index(Request $request): TagsResource
    {
        /** @var Builder $tag */
        $tag = Tag::select(['*']);
        if( $request->has('q') ){
            $tag_name = $request->input('q','');
            $tag->where('name', 'like', '%'.$tag_name.'%');
        }
        $presenter = new TagsResource( $tag->get() );
        $presenter->additional(['success' => true]);
        return $presenter;
    }

    /**
     * Store a newly created tag in storage.
     *
     * @param  TagRequest  $request
     * @return JsonResource
     */
    public function store(TagRequest $request): JsonResource
    {
        /** @var Tag $tag */
        $tag = (new Tag() )->fill($request->toArray());
        if( $tag->save() ) {
            $presenter = new TagResource( $tag );
            $presenter->additional(['success' => true]);
            return $presenter;
        }
        return response()->json([
            'success' => false
        ]);
    }

    /**
     * Display the Tag information.
     *
     * @param  Tag $tag
     * @return TagResource
     */
    public function show(Tag $tag): TagResource
    {
        $presenter = new TagResource( $tag );
        $presenter->additional(['success' => true]);
        return $presenter;
    }

    /**
     * Update the tag resource in storage.
     *
     * @param  TagRequest  $request
     * @param  Tag $tag
     * @return JsonResource
     */
    public function update(TagRequest $request, Tag $tag): JsonResource
    {
        /** @var Tag $tag */
        $tag->fill($request->toArray());
        if( $tag->save() ) {
            $presenter = new TagResource( $tag );
            $presenter->additional(['success' => true]);
            return $presenter;
        }
        return response()->json([
            'success' => false
        ]);
    }

    /**
     * Remove the Tag from storage.
     *
     * Make Tag soft deletion - set time by record deleted
     *
     * @param  Tag $tag
     * @return JsonResponse
     * @throws \Exception
     */
    public function destroy(Tag $tag): JsonResponse
    {
        return response()->json([
            'success' => $tag->delete()
        ]);
    }

    /**
     * Display a listing of tags used in Cards by user
     *
     * @return TagsResource
     */
    public function userstags(): TagsResource
    {
        /** @var Collection $tags */
        $tags = Tag::with([
            'cards' => function(BelongsToMany $query){
                $query->where('user_id', Auth::user()->id);
            }
        ])->get();
        $presenter = new TagsResource( $tags );
        $presenter->additional(['success' => true]);
        return $presenter;
    }

}
