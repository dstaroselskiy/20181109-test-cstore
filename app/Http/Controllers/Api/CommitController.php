<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\CommitRequest;
use App\Http\Resources\Commit\CommitResource;
use App\Http\Resources\Commit\CommitsResource;
use App\Models\Card;
use App\Models\Commit;
use Auth;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class CommitController extends Controller
{
    /**
     * Display a listing of the cards commit.
     *
     * @param Request $request
     * @param Card $card
     * @return CommitsResource
     */
    public function index(Request $request, Card $card): CommitsResource
    {
        $presenter = new CommitsResource( $card->commits()->with(['user'])->get()  );
        $presenter->additional(['success' => true]);
        return $presenter;

    }

    /**
     * Store a newly created commit by card in storage.
     *
     * @param CommitRequest $request
     * @param Card $card
     * @return JsonResource
     */
    public function store(CommitRequest $request, Card $card): JsonResource
    {
        /** @var Commit $commit */
        $commit = ( new Commit() )->fill($request->toArray());
        $commit->user()->associate( Auth::user() )
            ->card()->associate( $card );
        if( $commit->save() ) {
            $presenter = new CommitResource( $commit );
            $presenter->additional(['success' => true]);
            return $presenter;
        }
        return response()->json([
            'success' => false
        ]);
    }

    /**
     * Display the card commit resource.
     *
     * @param Card $card
     * @param Commit $commit
     * @return CommitResource
     * @throws \Throwable
     */
    public function show(Card $card, Commit $commit): CommitResource
    {
        throw_unless( $card->id === $commit->card_id, new NotFoundHttpException() );
        $commit->load(['user']);
        $presenter = new CommitResource( $commit );
        $presenter->additional(['success' => true]);
        return $presenter;
    }

    /**
     * Update the cards commit resource in storage.
     * @param Request $request
     * @param Card $card
     * @param Commit $commit
     * @return JsonResource
     * @throws \Throwable
     */
    public function update(Request $request, Card $card, Commit $commit): JsonResource
    {
        throw_unless( $card->id === $commit->card_id, new NotFoundHttpException() );
        /** @var Commit $commit */
        $commit->load(['user'])
            ->fill($request->toArray());
        if( $commit->save() ) {
            $presenter = new CommitResource( $commit );
            $presenter->additional(['success' => true]);
            return $presenter;
        }
        return response()->json([
            'success' => false
        ]);
    }

    /**
     * Remove the cards commit resource from storage.
     *
     * @param  Commit $commit
     * @return JsonResponse
     * @throws \Exception
     * @throws \Throwable
     */
    public function destroy(Card $card, Commit $commit): JsonResponse
    {
        throw_unless( $card->id === $commit->card_id, new NotFoundHttpException() );
        return response()->json([
            'success' => $commit->delete()
        ]);
    }
}
