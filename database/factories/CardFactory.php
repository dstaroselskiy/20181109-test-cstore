<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Models\Card::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'content' => $faker->text,
        'type' => 'text',
        'active' => true,
    ];
});

$factory->define(App\Models\CardList::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'content' => [
            [
                'checked' => $faker->boolean(),
                'name' => $faker->text,
            ],
            [
                'checked' => $faker->boolean(),
                'name' => $faker->text,
            ],
            [
                'checked' => $faker->boolean(),
                'name' => $faker->text,
            ],
        ],
        'type' => 'checkbox',
        'active' => true,
    ];
});
