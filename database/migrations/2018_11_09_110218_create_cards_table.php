<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if ( !Schema::hasTable('cards') ) {
            Schema::create('cards',function(Blueprint $table){
                /* The record key */
                $table->increments('id');
                /* The relation with user */
                $table->unsignedInteger('user_id');
                $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
                /* The name/title by card */
                $table->string('name')->nullable();
                /* The type by card. Can by text or checkbox */
                $table->string('type', 10)->default('text');
                /* The active status by card. */
                $table->boolean('active')->default(1);
                /* The content by card. If card type is text, then consist text, or if card type is checkbox, consist json */
                $table->text('content')->nullable();
                /* Add default column by created and updated record */
                $table->timestamps();
                /* The time (flag) by record deleted to trash */
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cards');
    }
}
