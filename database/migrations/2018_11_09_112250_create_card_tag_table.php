<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCardTagTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if ( !Schema::hasTable('card_tag') ) {
            /* The table card_tag - use for relation ManyToMany by cards and tags records */
            Schema::create('card_tag', function (Blueprint $table) {
                /* The record key */
                $table->increments('id');
                /* The relation by cards */
                $table->unsignedInteger('card_id');
                $table->foreign('card_id')->references('id')->on('cards')->onDelete('cascade');
                /* The relation by tags */
                $table->unsignedInteger('tag_id');
                $table->foreign('tag_id')->references('id')->on('tags')->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('card_tag');
    }
}
