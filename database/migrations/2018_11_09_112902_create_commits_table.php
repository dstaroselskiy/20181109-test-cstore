<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if ( !Schema::hasTable('commits') ) {
            Schema::create('commits', function (Blueprint $table) {
                /* The record key */
                $table->increments('id');
                /* The relation with user */
                $table->unsignedInteger('user_id');
                $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
                /* The relation by cards */
                $table->unsignedInteger('card_id');
                $table->foreign('card_id')->references('id')->on('cards')->onDelete('cascade');
                /* The content by commit. */
                $table->text('content')->nullable();
                /* The default columns by created and updated record */
                $table->timestamps();
                /* The time (flag) by record deleted to trash */
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('commits');
    }
}
