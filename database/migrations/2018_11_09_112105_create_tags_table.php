<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if ( !Schema::hasTable('tags') ) {
            Schema::create('tags', function (Blueprint $table) {
                /* The record key */
                $table->increments('id');
                /* The title/name by tag */
                $table->string('name')->unique();
                /* The default columns by created and updated record */
                $table->timestamps();
                /* The time (flag) by record deleted to trash */
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tags');
    }
}
